from .base import *
import configparser

DEBUG = True

ALLOWED_HOSTS = ['*']

config = configparser.ConfigParser()
config_file = os.path.join(os.path.dirname(BASE_DIR), 'env.ini')
config.read(config_file)

DATABASES = {
    'default': {
        'ENGINE': config['UAT']['DATABASE_ENGINE'],
        'NAME': config['UAT']['DATABASE_NAME'],
        'USER': config['UAT']['DATABASE_USER'],
        'PASSWORD': config['UAT']['DATABASE_PASSWORD'],
        'HOST': config['UAT']['DATABASE_HOST'],
        'PORT': config['UAT']['DATABASE_PORT'],
    }
}
