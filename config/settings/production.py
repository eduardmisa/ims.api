from .base import *
import configparser

DEBUG = False

ALLOWED_HOSTS = ['*']

config = configparser.ConfigParser()
config_file = os.path.join(os.path.dirname(BASE_DIR), 'env.ini')
config.read(config_file)

DATABASES = {
    'default': {
        'ENGINE': config['PRODUCTION']['DATABASE_ENGINE'],
        'NAME': config['PRODUCTION']['DATABASE_NAME'],
        'USER': config['PRODUCTION']['DATABASE_USER'],
        'PASSWORD': config['PRODUCTION']['DATABASE_PASSWORD'],
        'HOST': config['PRODUCTION']['DATABASE_HOST'],
        'PORT': config['PRODUCTION']['DATABASE_PORT'],
    }
}
