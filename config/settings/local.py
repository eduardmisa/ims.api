from .base import *
import configparser

DEBUG = True

config = configparser.ConfigParser()
config_file = os.path.join(BASE_DIR, 'env.ini')
config.read(config_file)

DATABASES = {
    'default': {
        'ENGINE': config['LOCAL']['DATABASE_ENGINE'],
        'NAME': config['LOCAL']['DATABASE_NAME'],
        'USER': config['LOCAL']['DATABASE_USER'],
        'PASSWORD': config['LOCAL']['DATABASE_PASSWORD'],
        'HOST': config['LOCAL']['DATABASE_HOST'],
        'PORT': config['LOCAL']['DATABASE_PORT'],
    }
}
