from django.db import transaction
from src.entities import models


def all(id):
    try:
        with transaction.atomic():
            return models.Stock\
                         .objects\
                         .all()
    except Exception as ex:
        print(str(ex))
        return None


def get(id):
    try:
        with transaction.atomic():
            return models.Stock\
                         .objects\
                         .filter(id=id)\
                         .first()
    except Exception as ex:
        print(str(ex))
        return None


def insert(item):
    try:
        with transaction.atomic():
            return models.Stock\
                         .objects\
                         .create(item)
    except Exception as ex:
        print(str(ex))
        return None


def edit(item):
    try:
        with transaction.atomic():
            return models.Stock\
                         .objects\
                         .update(item)
    except Exception as ex:
        print(str(ex))
        return None


def delete(id):
    try:
        with transaction.atomic():
            return models.Stock\
                         .objects\
                         .filter(id=id)\
                         .delete()
    except Exception as ex:
        print(str(ex))
        return None
