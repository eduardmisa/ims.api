from django.apps import AppConfig


class DataLayerConfig(AppConfig):
    name = 'data_layer'
