from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def get_product_info():
    queryset = None
    try:
        return models.Product\
                     .objects\
                     .values(
                         prod_id=F('id'),
                         prod_no=F('product_no'),
                         prod_name=F('name'),
                         prod_description=F('description'),
                         prod_is_serialized=F('is_serialized'),
                         prod_brand=F('brand__name'),
                         prod_category=F('category__name'),
                         prod_unit=F('unit_material__name'),
                         prod_quota=F('minimum_quantity'),
                         total_available_price=F('minimum_quantity'))
    except Exception as ex:
        print(f"##ERROR##: get_product_info:")
        print(str(ex))
    return queryset


def list_inventory_stocks_IS():
    queryset = None
    try:
        queryset = models.StockItem\
                           .objects\
                           .filter(
                               stock__product_id=OuterRef('pk'),
                               status=enums.StockEnum.IN.value)\
                           .values('stock__product_id')\
                           .annotate(val=Count('id')).values('val')
    except Exception as ex:
        print(f"##ERROR##: list_inventory_stocks_IS:")
        print(str(ex))
    return queryset


def list_inventory_stocks_PRI():
    queryset = None
    try:
        SUB_qty_pri_rejected = models.RequisitionRejectItem\
                                     .objects\
                                     .filter(
                                         requisition_product_id=OuterRef(
                                             'pk'))\
                                     .values('requisition_product_id')\
                                     .annotate(
                                         val=Coalesce(Sum('quantity'), 0))\
                                     .values('val')

        SUB_qty_pri_checkouts = models.CheckoutItem\
                                      .objects\
                                      .filter(
                                          requisition_product_id=OuterRef(
                                              'pk'))\
                                      .values('requisition_product_id')\
                                      .annotate(val=Coalesce(Count('id'), 0))\
                                      .values('val')

        queryset = models.RequisitionProduct\
                         .objects\
                         .filter(
                             product=OuterRef('pk'),
                             status__in=(
                                 enums.RequisitionEnum.PENDING.value,
                                 enums.RequisitionEnum.APPROVED.value,
                                 enums.RequisitionEnum.APPROVED_PARTIAL.value,
                                 enums.RequisitionEnum.REJECTED_PARTIAL.value,
                                 enums.RequisitionEnum.APPROVED_REJECTED_PARTIAL.value,
                             ))\
                         .values('product_id')\
                         .annotate(
                             val=Coalesce(Sum('quantity'), 0) -
                             (
                                 Coalesce(Sum(Subquery(
                                     SUB_qty_pri_rejected,
                                     output_field=IntegerField()),
                                 ), 0)
                                 +
                                 Coalesce(Sum(Subquery(
                                     SUB_qty_pri_checkouts,
                                     output_field=IntegerField())
                                 ), 0)
                             )
                         ).values('val')
    except Exception as ex:
        print(f"##ERROR##: list_inventory_stocks_PRI:")
        print(str(ex))
    return queryset


def list_inventory_stocks():
    queryset = None
    try:
        SUB_qty_is = list_inventory_stocks_IS()

        SUB_qty_pri = list_inventory_stocks_PRI()

        queryset = models.Product\
                         .objects\
                         .values(
                             prod_id=F('id'),
                             prod_no=F('product_no'),
                             prod_name=F('name'),
                             prod_brand=F('brand__name'),
                             prod_category=F('category__name'),
                             prod_unit=F('unit_material__name'),
                             qty_is=Coalesce(Sum(
                                        Subquery(
                                            SUB_qty_is,
                                            output_field=IntegerField()
                                        )), 0),
                             qty_pri=Coalesce(Sum(
                                         Subquery(
                                             SUB_qty_pri,
                                             output_field=IntegerField()
                                         )), 0),
                             qty_vb=F('qty_is')-F('qty_pri'),
                             is_critical=Case(
                                 When(qty_is__lte=F('minimum_quantity'),
                                      then=Value(True)),
                                 default=Value(False),
                                 output_field=BooleanField()
                             )
                         )
    except Exception as ex:
        print(f"##ERROR##: list_inventory_stocks:")
        print(str(ex))
    return queryset


def list_in_store_by_location(product_id):
    queryset = None
    try:
        queryset = models.StockItem\
                         .objects\
                         .filter(
                             stock__product_id=product_id,
                             status=enums.StockEnum.IN.value)\
                         .values(
                             current_warehouse=F('warehouse__name'),
                             unit_material=F('stock__product__unit_material__name'))\
                         .annotate(
                             qty_n=Sum(Case(When(item_condition__code=enums.ItemConditionEnum.NEW.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_u=Sum(Case(When(item_condition__code=enums.ItemConditionEnum.USED.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_t=F('qty_n') + F('qty_u'))
    except Exception as ex:
        print(f"##ERROR##: list_in_store_location:")
        print(str(ex))
    return queryset


def list_in_store_by_location_batch(product_id):
    queryset = None
    try:
        queryset = models.StockItem\
                         .objects\
                         .filter(
                             stock__product_id=product_id,
                             status=enums.StockEnum.IN.value)\
                         .values(
                             batch=F('acquisition_item__acquisition__acquisition_no'),
                             current_warehouse=F('warehouse__name'),
                             stock_currency=F('currency__name'),
                             acquired_date=F('acquisition_item__acquisition__acquired_date'),
                             po_no=F('acquisition_item__acquisition__po_no'),
                             inv_no=F('acquisition_item__acquisition__inv_no'),
                             unit_material=F('stock__product__unit_material__name'),
                             supplier=F('acquisition_item__acquisition__supplier__name'))\
                         .annotate(
                             qty_n=Sum(Case(When(item_condition__code=enums.ItemConditionEnum.NEW.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_u=Sum(Case(When(item_condition__code=enums.ItemConditionEnum.USED.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_t=F('qty_n') + F('qty_u'),
                             total_price=Sum(F('price')))
    except Exception as ex:
        print(f"##ERROR##: list_in_store_location_batch:")
        print(str(ex))
    return queryset


def list_in_store_by_location_batch_serialized_items(product_id,
                                                     acquisition_id,
                                                     warehouse):
    queryset = None
    try:
        queryset = models.StockItem\
                         .objects\
                         .filter(
                             acquisition_item__acquisition_id=acquisition_id,
                             warehouse_id=warehouse,
                             stock__product_id=product_id,
                             status=enums.StockEnum.IN.value)\
                         .values(
                             stockId=F('stock__id'),
                             serial_no=F('stock__serial_number'),
                             condition_i=F('item_condition_id'),
                             condition_d=F('item_condition__name'),
                             warehouse_i=F('warehouse_id'),
                             price_d=F('price'),
                             currency_d=F('currency__name'))
    except Exception as ex:
        print(f"##ERROR##: list_in_store_location_batch_serialized_items:")
        print(str(ex))
    return queryset


def transaction_in_store_serialized_edit():
    queryset = None
    try:
        queryset = models.Product.objects.values()
    except Exception as ex:
        print(f"##ERROR##: transaction_in_store_serialized_edit:")
        print(str(ex))
    return queryset


def transaction_in_store_serialized_delete():
    queryset = None
    try:
        queryset = models.Product.objects.values()
    except Exception as ex:
        print(f"##ERROR##: transaction_in_store_serialized_delete:")
        print(str(ex))
    return queryset


def transaction_in_store_serialized_shift_batch():
    queryset = None
    try:
        queryset = models.Product.objects.values()
    except Exception as ex:
        print(f"##ERROR##: transaction_in_store_serialized_shift_batch:")
        print(str(ex))
    return queryset


def transaction_in_store_serialized_shift_location():
    queryset = None
    try:
        queryset = models.Product.objects.values()
    except Exception as ex:
        print(f"##ERROR##: transaction_in_store_serialized_shift_location:")
        print(str(ex))
    return queryset


def transaction_in_store_consumable():
    queryset = None
    try:
        queryset = models.Product.objects.values()
    except Exception as ex:
        print(f"##ERROR##: transaction_in_store_consumable:")
        print(str(ex))
    return queryset


def list_batch_history(product_id):
    queryset = None
    try:
        queryset = models.AcquisitionItem\
                         .objects\
                         .filter(stock__product_id=product_id)\
                         .values(
                             acq_id=F('acquisition_id'),
                             batch_no=F('acquisition__acquisition_no'),
                             acquired_date=F('acquisition__acquired_date'),
                             currency_d=F('currency__name'),
                             po_no=F('acquisition__po_no'),
                             inv_no=F('acquisition__inv_no'),
                             supplier_d=F('acquisition__supplier'),
                             total_price=F('acquisition__batch_price'))\
                         .annotate(
                             qty=Count('id'))
    except Exception as ex:
        print(f"##ERROR##: list_batch_history:")
        print(str(ex))
    return queryset


def list_checkouts(product_id):
    queryset = None
    try:
        SUB_error_returns = models.CheckoutErrorReturnItem\
                                  .objects\
                                  .filter(checkout_item_id=OuterRef('pk'))\
                                  .values('checkout_item_id')

        SUB_returns = models.CheckoutReturnItem\
                            .objects\
                            .filter(checkout_item_id=OuterRef('pk'))\
                            .values('checkout_item_id')

        queryset = models.CheckoutItem\
                         .objects\
                         .filter(
                             ~Q(id__in=Subquery(SUB_error_returns)) |
                             ~Q(id__in=Subquery(SUB_returns)),
                             stock_item__stock__product_id=product_id)\
                         .values(
                             deployment_date=F('checkout__checkeout_on'),
                             location=F('checkout__location_from__name'),
                             project=F('checkout__location__name'),
                             requisition_no=F('checkout__requisition__requisition_no'),
                             issued_by=F('checkout__issued_by__username'),
                             checkout_by=F('checkout__checkeout_by__username'),
                             # serial_numbers=F(''),
                             requestor_remarks=F('checkout__requisition__remarks'),
                             remarks_collector=F('checkout__remarks'))\
                         .annotate(
                             qty_n=Sum(Case(When(stock_item__item_condition__code=enums.ItemConditionEnum.NEW.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_u=Sum(Case(When(stock_item__item_condition__code=enums.ItemConditionEnum.USED.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_t=F('qty_n') + F('qty_u'))
    except Exception as ex:
        print(f"##ERROR##: list_checkouts:")
        print(str(ex))
    return queryset


# TODO: THIS IS STILL ERROR
def list_returns(product_id):
    queryset = None
    try:
        queryset = models.CheckoutReturnItem\
                         .objects\
                         .filter()\
                         .values(
                             chk_rtn_id=F(
                                 'checkout_return_id'),
                             requisition_no=F(
                                 'checkout_item__checkout__checkeout_on'),
                             return_date=F(
                                 'checkout_item__checkout__checkeout_on'),
                             to_warehouse=F(
                                 'checkout_item__checkout__checkeout_on'),
                             from_project=F(
                                 'checkout_item__checkout__checkeout_on'),
                             return_by_user=F(
                                 'checkout_item__checkout__checkeout_on'))\
                         .annotate(
                             qty_n=Sum(Case(When(checkout_item__stock_item__item_condition__code=enums.ItemConditionEnum.NEW.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_u=Sum(Case(When(checkout_item__stock_item__item_condition__code=enums.ItemConditionEnum.USED.value,
                                                 then=Value(1)),
                                            default=Value(0),
                                            output_field=IntegerField())),
                             qty_t=F('qty_n') + F('qty_u'))
    except Exception as ex:
        print(f"##ERROR##: list_returns:")
        print(str(ex))
    return queryset
