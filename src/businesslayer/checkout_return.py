import copy
from datetime import datetime
from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from src.datalayer import checkout, checkout_return
from src.businesslayer.requisition import can_change_status
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def checkout_return_save(validated_data, user):
    error = None
    try:
        dateStamp = datetime.now()

        warehouse = validated_data['warehouse']
        project_location = validated_data['project_location']
        # checkout = validated_data['checkout']

        header = models.CheckoutReturn(
            remarks=validated_data['remarks'],
            return_by=user,
            return_on=dateStamp,
            warehouse=warehouse,
            project_location=project_location,
            # checkout=checkout,
        )

        header.save()

        checkout_return_items = validated_data.pop('checkout_return_items')

        for item in checkout_return_items:

            checkout_item = item['checkout_item']
            stock_item = checkout_item.stock_item

            if checkout_item.checkout_return_items.all().count() > 0\
               or\
               checkout_item.checkout_error_return_items.all().count() > 0:
                return f"Checkout Item [{checkout_item}] was already Returned"

            if stock_item.status != enums.StockEnum.OUT.value:
                return f"Stock [{stock_item}] "\
                       f"was not checked out"

            models.CheckoutReturnItem(
                checkout_item=checkout_item,
                checkout_return=header,
            ).save()

            stock_item.status = enums.StockEnum.IN.value
            stock_item.save()

    except Exception as ex:
        print(f"##ERROR##: checkout_return_save:")
        print(str(ex))
        error = str(ex)
    return error
