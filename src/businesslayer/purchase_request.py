from datetime import datetime
from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def requested_pr_count(product_id, requisition_id):
    return models.PurchaseRequestItem\
                 .objects\
                 .filter(
                     product=product_id,
                     purchase_request__requisition_id=requisition_id)\
                 .values('purchase_request__requisition_id')\
                 .annotate(val=Sum('quantity'))\
                 .values('val')
