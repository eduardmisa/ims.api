from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def supplier_stocks(supplier_id):
    queryset = None
    try:
        return models.AcquisitionItem\
            .objects\
            .filter(acquisition__supplier_id=supplier_id)\
            .values(
                product_no=F('stock__product__product_no'),
                product_name=F('stock__product__name'),
                product_description=F('stock__product__description'),
                product_brand=F('stock__product__brand__name'),
                product_category=F('stock__product__category__name'))\
            .annotate(
                count=Count(F('stock__product_id')))
    except Exception as ex:
        print(f"##ERROR##: supplier_stocks:")
        print(str(ex))

    return queryset


def clients_with_project_count():
    queryset = None
    try:
        return models.Client\
            .objects\
            .annotate(
                no_of_projects=Count(F('projects')))
    except Exception as ex:
        print(f"##ERROR##: clients_with_project_count:")
        print(str(ex))

    return queryset
