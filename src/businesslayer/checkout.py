import copy
from datetime import datetime
from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from src.datalayer import requisition
from src.businesslayer.requisition import can_change_status
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def generate_next_series(id):
    return f"{enums.ModelSeriesEnum.CHECKOUT.value}-{id}"


# TODO: Handle Closing of Requisition "checkout_close = TRUE" - Done
# TODO: Handle Checking out quantity grated than the approved count - Done
def checkout_save(validated_data, user):
    error = None
    try:
        dateStamp = datetime.now()

        requisition = validated_data['requisition']

        if not can_change_status(requisition.status):
            return f"Requisition {requisition} is {requisition.status}"

        if requisition.checkout_close:
            return f"Requisition [{requisition}] is already "\
                   f"Closed for checkout"

        header = models.Checkout(
            checkout_no="TEMP",
            remarks=validated_data['remarks'],
            location=validated_data['location'],
            location_from=validated_data['location_from'],
            issued_by=validated_data['issued_by'],
            checkeout_on=dateStamp,
            checkeout_by=validated_data['checkeout_by'],
            requisition=requisition,
        )

        header.save()
        header.checkout_no = generate_next_series(header.id)
        header.save()

        checkout_items = validated_data.pop('checkout_items')

        for item in checkout_items:

            stock_item = item['stock_item']
            requisition_product = item['requisition_product']

            if not can_checkout(requisition_product.status):
                return f"Requisition Product {requisition} "\
                       f"is {requisition_product.status}"

            if stock_item.status != enums.StockEnum.IN.value:
                return f"Stock [{stock_item}] is not IN stock"

            models.CheckoutItem(
                stock_item=stock_item,
                requisition_product=requisition_product,
                checkout=header,
            ).save()

            if requisition_product\
                .requisitions_approve_items\
                .all()\
                .aggregate(val=Coalesce(Sum('quantity'), 0))['val']\
               <\
               requisition_product.checkout_items.all().count():
                return f"Checkout quantity will exceed the Approved quantity "\
                       f"for requested product [{requisition_product}]"

            stock_item.status = enums.StockEnum.OUT.value
            stock_item.save()

        # TODO: Check if Already Closed for Checkout
        #
        # Steps for each Requested Product:
        #   Get Total Requested Quantity
        #   Minus the Rejected Quantity
        #   if and only if, the Difference is == Checked out Quantity
        #       REQUESTED PRODUCT CLOSED;
        #   if All Requested Product is CLOSED
        #       CLOSED;
        for item in requisition.requisition_products.all():
            requestedTotal = item.quantity
            rejectedTotal = item.requisitions_reject_items\
                                .all()\
                                .aggregate(val=Coalesce(
                                    Sum('quantity'), 0))['val']
            # approvedTotal = item.requisitions_approve_items\
            #                     .all()\
            #                     .aggregate(val=Coalesce(
            #                         Sum('quantity'), 0))['val']
            checkoutTotal = item.checkout_items\
                                .all()\
                                .aggregate(val=Coalesce(
                                    Count('id'), 0))['val']
            if (requestedTotal-rejectedTotal) == checkoutTotal:
                requisition.checkout_close = True
                requisition.save()
            else:
                requisition.checkout_close = False
                requisition.save()
                break

    except Exception as ex:
        print(f"##ERROR##: checkout_save:")
        print(str(ex))
        error = str(ex)
    return error


"""
************************************
*********| Helper Methods |*********
************************************
"""


def can_checkout(status):
    return status != enums.RequisitionEnum\
                          .APPROVED.value\
           or\
           status != enums.RequisitionEnum\
                          .APPROVED_PARTIAL.value\
           or\
           status != enums.RequisitionEnum\
                          .APPROVED_REJECTED_PARTIAL.value
