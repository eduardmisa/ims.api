from django.apps import AppConfig


class BusinessLayerConfig(AppConfig):
    name = 'business_layer'
