import copy
from datetime import datetime
from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from src.businesslayer import inventory
from src.datalayer import requisition
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def generate_next_series(id):
    return f"{enums.ModelSeriesEnum.REQUISITION.value}-{id}"


def generate_next_series_pr(id):
    return f"{enums.ModelSeriesEnum.PURCHASEREQUEST.value}-{id}"


def stocks_for_requisition(all):
    queryset = None
    try:

        SUB_qty_total = inventory.list_inventory_stocks_IS()
        SUB_qty_total = SUB_qty_total.filter(warehouse__is_stock_visible=all)

        SUB_qty_pri = inventory.list_inventory_stocks_PRI()

        queryset = models.Product.objects.values(
            product=F('id'),
            productIsSerialized=F('is_serialized'),
            productNo=F('product_no'),
            productName=F('name'),
            productBrand=F('brand__name'),
            productCategory=F('category__name'),
            productUnit=F('unit_material__name'),
            qty_total=Coalesce(
                Sum(Subquery(SUB_qty_total, output_field=IntegerField())), 0),
            qty_pri=Coalesce(
                Sum(Subquery(SUB_qty_pri, output_field=IntegerField())), 0),
            qty_vb=F('qty_total')-F('qty_pri')
        )
    except Exception as ex:
        print(f"##ERROR##: stocks_for_requisition:")
        print(str(ex))
    return queryset


def requisition_save(validated_data, user):
    error = []
    try:
        dateStamp = datetime.now()

        dateRequiredForPurchasing = None
        if 'purchase_required_date' in validated_data:
            dateRequiredForPurchasing =\
                validated_data.pop('purchase_required_date')

        requisition_products = validated_data.pop('requisition_products')

        if len(requisition_products) == 0:
            raise Exception('Requisition Items for Product are required')

        # REQUISITION HEADER
        header = models.Requisition(
            status=enums.RequisitionEnum.PENDING.value,
            requisition_no="TEMP",
            requestor=user,
            requested_date=dateStamp,
            approver=validated_data['approver'],
            checkout_close=False,
            approve_as_urgent=False,
            project=validated_data['project'],
            remarks=validated_data['remarks']
        )
        header.save()
        header.requisition_no = generate_next_series(header.id)
        header.save()

        # Check if there is items for Purcase Request
        hasPR = False
        prHeader = None

        for item in requisition_products:
            if 'qty_pr' in item and int(item['qty_pr']) > 0:
                hasPR = True

                if dateRequiredForPurchasing is None:
                    return 'Date Required For Purchasing is required'
                elif dateRequiredForPurchasing < dateStamp:
                    return 'Invalid DateRequiredForPurchasing'

        if hasPR:
            savedPR = models.PurchaseRequest(
                status=enums.RequisitionEnum.PENDING.value,
                pr_no="TEMP",
                po_no="",
                inv_no="",
                remarks=header.remarks,
                requestor=user,
                requested_date=dateStamp,
                required_date=dateRequiredForPurchasing,
                requisition=header
            )
            savedPR.save()
            savedPR.pr_no = generate_next_series_pr(savedPR.id)
            savedPR.save()

        for item in requisition_products:

            qty_sr = int(item.pop('qty_sr'))
            qty_pr = int(item.pop('qty_pr'))

            # SAVE REQUISITION PRODUCT
            if qty_sr > 0:

                product = item['product']

                # TODO: Validate For Requisition Product Here - Done

                requisition_product_db = stocks_for_requisition(True)\
                    .filter(product=product.id)\
                    .first()

                if requisition_product_db['qty_vb'] < qty_sr:
                    error.append({
                        f"{product}": {
                            "requested": qty_sr,
                            "available": requisition_product_db['qty_vb']
                        }
                    })

                models.RequisitionProduct(
                    status=enums.RequisitionEnum.PENDING.value,
                    quantity=qty_sr,
                    remarks=header.remarks,
                    product=product,
                    requisition=header
                ).save()

            # SAVE PURCHASE REQUEST PRODUCT
            if qty_pr > 0 and savedPR is not None:
                models.PurchaseRequestItem(
                    status=enums.RequisitionEnum.PENDING.value,
                    quantity=qty_pr,
                    remarks=header.remarks,
                    product=item['product'],
                    purchase_request=savedPR
                ).save()

        if len(error) > 0:
            return error

    except Exception as ex:
        print(f"##ERROR##: requisition_save:")
        print(str(ex))
        error = str(ex)
    return error


def list_sr_Items(requisition_id):
    queryset = None
    try:
        subqryQty_c_returns = models.CheckoutErrorReturnItem\
                                    .objects\
                                    .filter(checkout_item=OuterRef('pk'))\
                                    .values(
                                        'checkout_item__requisition_product')\
                                    .annotate(qty_c_t_r=Count('id'))\
                                    .values('qty_c_t_r')

        subqryQty_c = models.CheckoutItem\
                            .objects\
                            .filter(
                                checkout__requisition=requisition_id,
                                requisition_product=OuterRef('pk'))\
                            .values('requisition_product')\
                            .annotate(
                                qty_c_t=Count('id')-Sum(
                                    Coalesce(
                                        Subquery(subqryQty_c_returns,
                                                 output_field=IntegerField()),
                                        0)))\
                            .values('qty_c_t')

        queryset = models.RequisitionProduct\
                         .objects\
                         .filter(requisition=requisition_id)\
                         .values(
                             row_id=F('id'),
                             product_no=F('product__product_no'),
                             product_name=F('product__name'),
                             brand=F('product__brand__name'),
                             category=F('product__category__name'),
                             req_status=F('status'),
                             unit=F('product__unit_material__name'),
                             location=F('product__unit_material__name'))\
                         .annotate(
                             qty_sr=Coalesce(Sum('quantity'), 0),
                             qty_c=Coalesce(Sum(Subquery(
                                 subqryQty_c,
                                 output_field=IntegerField())), 0),
                             qty_rc=Coalesce(F('qty_sr') - F('qty_c'), 0))

    except Exception as ex:
        print(f"##ERROR##: list_sr_Items:")
        print(str(ex))
    return queryset


def list_pr_Items(requisition_id):
    queryset = None
    try:
        queryset = models.PurchaseRequestItem\
                        .objects\
                        .filter(
                            purchase_request__requisition=requisition_id)\
                        .values(
                            row_id=F('id'),
                            pr_header_id=F('purchase_request_id'),
                            product_no=F('product__product_no'),
                            product_name=F('product__name'),
                            product_brand=F('product__brand__name'),
                            product_category=F('product__category__name'),
                            product_unit=F('product__unit_material__name'),
                            pr_item_status=F('status'),
                            qty_pr=F('quantity'),
                            )

    except Exception as ex:
        print(f"##ERROR##: list_pr_Items:")
        print(str(ex))
    return queryset


def list_ck_Items(requisition_id):
    queryset = None
    try:
        queryset = models.Checkout\
                            .objects\
                            .filter(requisition=requisition_id) \
                            .values(
                                d_checkout_no=F('ck_no'),
                                d_checkout_on=F('checkeout_on'),
                                d_checkout_from_location=F('location_from'),
                                d_checkout_issued_by=F('issued_by'),
                                d_checkout_checkout_by=F('checkeout_by'),
                                d_checkout_remarks=F('remarks'))

    except Exception as ex:
        print(f"##ERROR##: list_ck_Items:")
        print(str(ex))
    return queryset


def requisition_cancel(requisition_id, user):
    error = None
    try:
        dateStamp = datetime.now()

        instance = models.Requisition\
                         .objects\
                         .filter(id=requisition_id)\
                         .first()

        if instance is None:
            return "Requisition not found"

        if instance.status != enums.RequisitionEnum.PENDING.value:
            return f"Requisition is already {instance.status}"

        instance.status = enums.RequisitionEnum.CANCELED.value
        instance.save()

        # instance.purchase_requests
        for purchase_requests_item in instance.purchase_requests.all():
            purchase_requests_item.status = enums.RequisitionEnum\
                                                 .CANCELED.value
            purchase_requests_item.save()

            for i in purchase_requests_item.purchase_request_items.all():
                i.status = enums.RequisitionEnum.CANCELED.value
                i.save()

        # instance.requisition_products
        for requisition_products_item in instance.requisition_products.all():
            requisition_products_item.status = enums.RequisitionEnum\
                                                    .CANCELED.value
            requisition_products_item.save()

        # Insert to ForceClose Table
        models.RequisitionForceClose(
            close_remarks='SYSTEM: cancel request',
            closed_by=user,
            closed_on=dateStamp,
            requisition=instance
        ).save()

    except Exception as ex:
        print(f"##ERROR##: requisition_cancel:")
        print(str(ex))
        error = str(ex)
    return error


def requisition_force_close(requisition_id, remarks, user):
    error = None
    try:
        dateStamp = datetime.now()

        instance = models.Requisition\
                         .objects\
                         .filter(id=requisition_id)\
                         .first()

        if instance is None:
            return "Requisition not found"

        # if instance.status is not enums.RequisitionEnum.PENDING.value:
        #     return f"Requisition is already {instance.status}"

        instance.status = enums.RequisitionEnum.FORCE_CLOSED.value
        instance.save()

        # instance.purchase_requests
        for purchase_requests_item in instance.purchase_requests.all():
            purchase_requests_item.status = enums.RequisitionEnum\
                                                 .FORCE_CLOSED.value
            purchase_requests_item.save()

            for i in purchase_requests_item.purchase_request_items.all():
                i.status = enums.RequisitionEnum.FORCE_CLOSED.value
                i.save()

        # instance.requisition_products
        for requisition_products_item in instance.requisition_products.all():
            requisition_products_item.status = enums.RequisitionEnum\
                                                    .FORCE_CLOSED.value
            requisition_products_item.save()

        # Insert to ForceClose Table
        models.RequisitionForceClose(
            close_remarks=remarks,
            closed_by=user,
            closed_on=dateStamp,
            requisition=instance
        ).save()

    except Exception as ex:
        print(f"##ERROR##: requisition_force_close:")
        print(str(ex))
        error = str(ex)
    return error


def requisition_reject(validated_data, user):
    error = None
    try:
        dateStamp = datetime.now()

        #############################
        # Reject Stock Requisitions #
        #############################

        if 'sr_reject' in validated_data:

            sr_reject = validated_data['sr_reject']

            requisition = sr_reject['requisition']

            if can_change_status(requisition.status):
                return f"Requisition is already {requisition.status}"

            requisition_reject = models.RequisitionReject(
                remarks=sr_reject['remarks'],
                rejected_by=user,
                rejected_on=dateStamp,
                requisition=requisition
            )

            requisition_reject.save()

            for item in sr_reject['requisitions_reject_items']:

                requisition_product = item['requisition_product']

                if can_change_status(requisition_product.status):
                    return f"Product Requested [{requisition_product}] "\
                           f"is already {requisition_product.status}"

                models.RequisitionRejectItem(
                    quantity=item['quantity'],
                    remarks=item['remarks'],
                    requisition_reject=requisition_reject,
                    requisition_product=requisition_product
                ).save()

                # Change Requested Product Status
                try:
                    requisition_product_status(requisition_product)
                    requisition_product.save()
                except:
                    return f"Product Requested "\
                           f"[{requisition_product}] "\
                           f"exceeded the rejection count."

            approvedRequisitionProducts =\
                list(requisition.requisition_products
                                .all()
                                .values('status')
                                .distinct())

            requisition.status = status_identifier(
                [x['status'] for x in approvedRequisitionProducts])

            requisition.save()

        ############################
        # Reject Purchase Requests #
        ############################

        if 'pr_reject' in validated_data:

            pr_reject = validated_data['pr_reject']

            purchase_request = pr_reject['purchase_request']

            if can_change_status(purchase_request.status):
                return f"Purchase Request is already "\
                       f"{purchase_request.status}"

            purchase_request_reject = models.PurchaseRequestReject(
                remarks=pr_reject['remarks'],
                rejected_by=user,
                rejected_on=dateStamp,
                purchase_request=purchase_request
            )

            purchase_request_reject.save()

            for item in pr_reject['purchase_requests_reject_items']:

                purchase_request_item = item['purchase_request_item']

                if can_change_status(purchase_request_item.status):
                    return f"Purchase Requested [{purchase_request_item.id}]"\
                           f"is already {purchase_request_item.status}"

                models.PurchaseRequestRejectItem(
                    quantity=item['quantity'],
                    remarks=item['remarks'],
                    purchase_request_reject=purchase_request_reject,
                    purchase_request_item=purchase_request_item
                ).save()

                # Change Requested Product Status
                try:
                    purchase_request_item_status(purchase_request_item)
                    purchase_request_item.save()
                except:
                    return f"Purchase Request Product "\
                           f"[{purchase_request_item}] "\
                           f"exceeded the rejection count."

            rejectedPRItems =\
                list(purchase_request.purchase_request_items
                                     .all()
                                     .values('status')
                                     .distinct())

            purchase_request.status = status_identifier(
                [x['status'] for x in rejectedPRItems])

            purchase_request.save()

    except Exception as ex:
        print(f"##ERROR##: requisition_reject:")
        print(str(ex))
        error = str(ex)
    return error


def requisition_approve_sr_items(requisition_id):
    queryset = None
    try:
        SUB_approved_count = models.RequisitionApproveItem\
                                   .objects\
                                   .filter(
                                       requisition_product_id=OuterRef('pk'))\
                                   .values('requisition_product_id')\
                                   .annotate(val=Sum('quantity'))\
                                   .values('val')
        SUB_rejected_count = models.RequisitionRejectItem\
                                   .objects\
                                   .filter(
                                       requisition_product_id=OuterRef('pk'))\
                                   .values('requisition_product_id')\
                                   .annotate(val=Sum('quantity'))\
                                   .values('val')

        queryset = models.RequisitionProduct\
                         .objects\
                         .filter(requisition=requisition_id)\
                         .values(
                             row_id=F('id'),
                             product_no=F('product__product_no'),
                             product_name=F('product__name'),
                             product_unit=F('product__unit_material__name'),
                             req_status=F('status'),
                             qty_remaining=F('quantity') -
                             (
                                Coalesce(Sum(Subquery(SUB_approved_count, output_field=IntegerField())), 0)
                                +
                                Coalesce(Sum(Subquery(SUB_rejected_count, output_field=IntegerField())), 0)
                             ))

    except Exception as ex:
        print(f"##ERROR##: requisition_approve_sr_items:")
        print(str(ex))
    return queryset


def requisition_approve_pr_items(requisition_id):
    queryset = None
    try:
        SUB_approved_count = models.PurchaseRequestApproveItem\
                                   .objects\
                                   .filter(
                                       purchase_request_item_id=OuterRef('pk')
                                    )\
                                   .values('purchase_request_item_id')\
                                   .annotate(val=Sum('quantity'))\
                                   .values('val')
        SUB_rejected_count = models.PurchaseRequestRejectItem\
                                   .objects\
                                   .filter(
                                       purchase_request_item_id=OuterRef('pk')
                                    )\
                                   .values('purchase_request_item_id')\
                                   .annotate(val=Sum('quantity'))\
                                   .values('val')

        queryset = models.PurchaseRequestItem\
                         .objects\
                         .filter(
                             purchase_request__requisition=requisition_id)\
                         .values(
                             row_id=F('id'),
                             pr_header_id=F('purchase_request_id'),
                             product_no=F('product__product_no'),
                             product_name=F('product__name'),
                             product_unit=F('product__unit_material__name'),
                             req_status=F('status'),
                             qty_remaining=F('quantity') -
                             (
                                 Sum(Coalesce(Subquery(SUB_approved_count,
                                              output_field=IntegerField()), 0))
                                 +
                                 Sum(Coalesce(Subquery(SUB_rejected_count,
                                              output_field=IntegerField()), 0))
                             ))

    except Exception as ex:
        print(f"##ERROR##: requisition_approve_pr_items:")
        print(str(ex))
    return queryset


def requisition_approve(validated_data, user):
    error = None
    try:
        dateStamp = datetime.now()

        ##############################
        # Approve Stock Requisitions #
        ##############################

        if 'sr_approve' in validated_data:

            sr_approve = validated_data['sr_approve']

            requisition = sr_approve['requisition']

            if can_change_status(requisition.status):
                return f"Requisition is already {requisition.status}"

            requisition_approve = models.RequisitionApprove(
                remarks=sr_approve['remarks'],
                approved_by=user,
                approved_on=dateStamp,
                requisition=requisition
            )

            requisition_approve.save()

            for item in sr_approve['requisitions_approve_items']:

                requisition_product = item['requisition_product']

                if can_change_status(requisition_product.status):
                    return f"Product Requested [{requisition_product}] "\
                           f"is already {requisition_product.status}"

                models.RequisitionApproveItem(
                    quantity=item['quantity'],
                    remarks=item['remarks'],
                    requisition_approve=requisition_approve,
                    requisition_product=requisition_product
                ).save()

                # Change Requested Product Status
                try:
                    requisition_product_status(requisition_product)
                    requisition_product.save()
                except Exception as ex:
                    return f"Product Requested "\
                           f"[{requisition_product}] "\
                           f"exceeded the approval count."

            approvedRequisitionProducts =\
                list(requisition.requisition_products
                                .all()
                                .values('status')
                                .distinct())

            requisition.status = status_identifier(
                [x['status'] for x in approvedRequisitionProducts])

            requisition.save()

        else:
            if 'pr_approve' not in validated_data:
                error = 'Requisition Items for Approval are required'

        #############################
        # Approve Purchase Requests #
        #############################

        if 'pr_approve' in validated_data:

            pr_approve = validated_data['pr_approve']

            purchase_request = pr_approve['purchase_request']

            if can_change_status(purchase_request.status):
                return f"Purchase Request is already "\
                       f"{purchase_request.status}"

            purchase_request_approve = models.PurchaseRequestApprove(
                remarks=pr_approve['remarks'],
                approved_by=user,
                approved_on=dateStamp,
                purchase_request=purchase_request
            )

            purchase_request_approve.save()

            for item in pr_approve['purchase_requests_approve_items']:

                purchase_request_item = item['purchase_request_item']

                if can_change_status(purchase_request_item.status):
                    return f"Purchase Requested [{purchase_request_item.id}]"\
                           f"is already {purchase_request_item.status}"

                models.PurchaseRequestApproveItem(
                    quantity=item['quantity'],
                    remarks=item['remarks'],
                    purchase_request_approve=purchase_request_approve,
                    purchase_request_item=purchase_request_item
                ).save()

                # Change Requested Product Status
                try:
                    purchase_request_item_status(purchase_request_item)
                    purchase_request_item.save()
                except:
                    return f"Purchase Request Product "\
                           f"[{purchase_request_item}] "\
                           f"exceeded the approval count."

            approvedPRItems =\
                list(purchase_request.purchase_request_items
                                     .all()
                                     .values('status')
                                     .distinct())

            purchase_request.status = status_identifier(
                [x['status'] for x in approvedPRItems])

            purchase_request.save()

    except Exception as ex:
        print(f"##ERROR##: requisition_approve:")
        print(str(ex))
        error = str(ex)
    return error


"""
************************************
*********| Helper Methods |*********
************************************
"""


# TODO: Handle REJECTED_PARTIAL and APPROVED_REJECTED_PARTIAL - Done
def status_identifier(STATUS_SET):

    if len(STATUS_SET) == 1\
       and\
       STATUS_SET[0] == enums.RequisitionEnum.PENDING.value:
        return enums.RequisitionEnum.PENDING.value

    if len(STATUS_SET) == 1\
       and\
       STATUS_SET[0] == enums.RequisitionEnum.APPROVED.value:
        return enums.RequisitionEnum.APPROVED.value

    if len(STATUS_SET) == 1\
       and\
       STATUS_SET[0] == enums.RequisitionEnum.REJECTED.value:
        return enums.RequisitionEnum.REJECTED.value

    if len(STATUS_SET) == 1\
       and\
       STATUS_SET[0] == enums.RequisitionEnum.APPROVED_PARTIAL.value:
        return enums.RequisitionEnum.APPROVED_PARTIAL.value

    if len(STATUS_SET) == 1\
       and\
       STATUS_SET[0] == enums.RequisitionEnum.REJECTED_PARTIAL.value:
        return enums.RequisitionEnum.REJECTED_PARTIAL.value

    if len(STATUS_SET) == 1\
       and\
       STATUS_SET[0] == enums.RequisitionEnum.APPROVED_REJECTED_PARTIAL.value:
        return enums.RequisitionEnum.APPROVED_REJECTED_PARTIAL.value

    if len(STATUS_SET) == 2\
       and\
       enums.RequisitionEnum.PENDING.value in STATUS_SET\
       and\
       enums.RequisitionEnum.APPROVED.value in STATUS_SET:
        return enums.RequisitionEnum.APPROVED_PARTIAL.value

    if len(STATUS_SET) == 2\
       and\
       enums.RequisitionEnum.PENDING.value in STATUS_SET\
       and\
       enums.RequisitionEnum.REJECTED.value in STATUS_SET:
        return enums.RequisitionEnum.REJECTED_PARTIAL.value

    if len(STATUS_SET) == 2\
       and\
       enums.RequisitionEnum.PENDING.value in STATUS_SET\
       and\
       enums.RequisitionEnum.APPROVED_PARTIAL.value in STATUS_SET:
        return enums.RequisitionEnum.APPROVED_PARTIAL.value

    if len(STATUS_SET) == 2\
       and\
       enums.RequisitionEnum.PENDING.value in STATUS_SET\
       and\
       enums.RequisitionEnum.REJECTED_PARTIAL.value in STATUS_SET:
        return enums.RequisitionEnum.REJECTED_PARTIAL.value

    if len(STATUS_SET) == 2\
       and\
       enums.RequisitionEnum.APPROVED.value in STATUS_SET\
       and\
       enums.RequisitionEnum.APPROVED_PARTIAL.value in STATUS_SET:
        return enums.RequisitionEnum.APPROVED_PARTIAL.value

    if len(STATUS_SET) == 2\
       and\
       enums.RequisitionEnum.REJECTED.value in STATUS_SET\
       and\
       enums.RequisitionEnum.REJECTED_PARTIAL.value in STATUS_SET:
        return enums.RequisitionEnum.REJECTED_PARTIAL.value

    return enums.RequisitionEnum.APPROVED_REJECTED_PARTIAL.value


# Total Aproved and Rejected Count
def requisition_product_total(requisition_product):
    approvedCount =\
        requisition_product.requisitions_approve_items\
                           .all()\
                           .aggregate(val=Coalesce(Sum('quantity'), 0))
    rejectedCount =\
        requisition_product.requisitions_reject_items\
                           .all()\
                           .aggregate(val=Coalesce(Sum('quantity'), 0))
    return approvedCount['val'] + rejectedCount['val']


# Total Aproved and Rejected Count
def purchase_request_item_total(purchase_request_item):
    approvedCount =\
        purchase_request_item.purchase_requests_approve_items\
                             .all()\
                             .aggregate(val=Coalesce(Sum('quantity'), 0))
    rejectedCount =\
        purchase_request_item.purchase_requests_reject_items\
                             .all()\
                             .aggregate(val=Coalesce(Sum('quantity'), 0))
    return approvedCount['val'] + rejectedCount['val']


def requisition_product_status(requisition_product):

    approveCount = requisition_product\
        .requisitions_approve_items\
        .all()\
        .aggregate(val=Coalesce(Count('id'), 0))['val']

    rejectCount = requisition_product\
        .requisitions_reject_items\
        .all()\
        .aggregate(val=Coalesce(Count('id'), 0))['val']

    currentCount = requisition_product_total(requisition_product)

    if requisition_product.quantity == currentCount:
        if approveCount > 0 and rejectCount == 0:
            requisition_product.status = enums.RequisitionEnum\
                                              .APPROVED.value
        elif rejectCount > 0 and approveCount == 0:
            requisition_product.status = enums.RequisitionEnum\
                                              .REJECTED.value
        else:
            requisition_product.status = enums.RequisitionEnum\
                                              .APPROVED_REJECTED_PARTIAL.value
    elif requisition_product.quantity > currentCount:
        if approveCount > 0 and rejectCount == 0:
            requisition_product.status = enums.RequisitionEnum\
                                              .APPROVED_PARTIAL.value
        elif rejectCount > 0 and approveCount == 0:
            requisition_product.status = enums.RequisitionEnum\
                                              .REJECTED_PARTIAL.value
        else:
            requisition_product.status = enums.RequisitionEnum\
                                              .APPROVED_REJECTED_PARTIAL.value
    else:
        raise Exception('catch me')


def purchase_request_item_status(purchase_request_item):
    approveCount = purchase_request_item\
        .purchase_requests_approve_items\
        .all()\
        .aggregate(val=Coalesce(Count('id'), 0))['val']

    rejectCount = purchase_request_item\
        .purchase_requests_reject_items\
        .all()\
        .aggregate(val=Coalesce(Count('id'), 0))['val']

    currentCount = purchase_request_item_total(purchase_request_item)

    if purchase_request_item.quantity == currentCount:
        if approveCount > 0 and rejectCount == 0:
            purchase_request_item.status = enums.RequisitionEnum\
                                                .APPROVED.value
        elif rejectCount > 0 and approveCount == 0:
            purchase_request_item.status = enums.RequisitionEnum\
                                                .REJECTED.value
        else:
            purchase_request_item.status = enums.RequisitionEnum\
                                                .APPROVED_REJECTED_PARTIAL\
                                                .value
    elif purchase_request_item.quantity > currentCount:
        if approveCount > 0 and rejectCount == 0:
            purchase_request_item.status = enums.RequisitionEnum\
                                                .APPROVED_PARTIAL.value
        elif rejectCount > 0 and approveCount == 0:
            purchase_request_item.status = enums.RequisitionEnum\
                                                .REJECTED_PARTIAL.value
        else:
            purchase_request_item.status = enums.RequisitionEnum\
                                                .APPROVED_REJECTED_PARTIAL\
                                                .value
    else:
        raise Exception('catch me')


def can_change_status(status):
    return status != enums.RequisitionEnum\
                          .PENDING.value\
           and\
           status != enums.RequisitionEnum\
                          .APPROVED_PARTIAL.value\
           and\
           status != enums.RequisitionEnum\
                          .REJECTED_PARTIAL.value\
           and\
           status != enums.RequisitionEnum\
                          .APPROVED_REJECTED_PARTIAL.value
