from django.db import transaction
from src.entities import models, enums
from django.db.models.functions import Concat, Coalesce, Lower
from src.datalayer import acquisition
from django.db.models import (Q, F, Sum, OuterRef, Subquery, Q, Case, When,
                              Value, Func, Count,
                              CharField,
                              IntegerField,
                              DecimalField,
                              BooleanField)


def generate_next_series(id):
    return f"{enums.ModelSeriesEnum.ACQUISITION.value}-{id}"


def generate_next_series_srp(id):
    return f"{enums.ModelSeriesEnum.SERIALIZEPRODUCT.value}-{id}"


def generate_next_series_csp(id):
    return f"{enums.ModelSeriesEnum.CONSUMABLEPRODUCTS.value}-{id}"


def manual_add():
    queryset = None
    try:
        queryset = models.Acquisition\
                         .objects\
                         .all()
    except Exception as ex:
        print(f"##ERROR##: manual_add:")
        print(str(ex))
    return queryset


def manual_add_details(colAcqId):
    queryset = None
    try:
        return models.Stock\
                     .objects\
                     .filter(acquisition_items__acquisition_id=colAcqId)
    except Exception as ex:
        print(f"##ERROR##: manual_add:")
        print(str(ex))
    return queryset


def manual_add_save(validated_data, user):
    error = None
    try:
        error = "Stocks should not be empty"\
            if len(validated_data['stocks']) == 0\
            else None

        stocks_data = validated_data.pop('stocks')

        validated_data['acquisition_no'] = "TEMP"

        saved_acquisition = models.Acquisition\
                                  .objects\
                                  .create(**validated_data)

        saved_acquisition.acquisition_no = generate_next_series(
            saved_acquisition.id)

        saved_acquisition.save()

        for stock_data in stocks_data:

            acquisition_items_data = stock_data.pop('acquisition_items')

            stock_data['stock_no'] = "TEMP"

            saved_stock = models.Stock\
                                .objects\
                                .create(**stock_data)

            if saved_stock.product.is_serialized:
                saved_stock.stock_no = generate_next_series_srp(
                    saved_stock.id)
            else:
                saved_stock.stock_no = generate_next_series_csp(
                    saved_stock.id)

            saved_stock.save()

            for acquisition_item_data in acquisition_items_data:

                acquisition_item_data['stock'] = saved_stock

                acquisition_item_data['acquisition'] = saved_acquisition

                saved_acquisition_item = models.AcquisitionItem\
                                               .objects\
                                               .create(**acquisition_item_data)

                each_price = saved_acquisition_item.price /\
                    saved_acquisition_item.quantity

                for i in range(saved_acquisition_item.quantity):
                    models.StockItem(
                        status=enums.StockEnum.IN.value,
                        price=each_price,
                        currency=saved_acquisition_item.currency,
                        item_condition=saved_acquisition_item.item_condition,
                        warehouse=saved_acquisition_item.acquisition.warehouse,
                        stock=saved_acquisition_item.stock,
                        acquisition_item=saved_acquisition_item
                    ).save()

    except Exception as ex:
        print(f"##ERROR##: manual_add:")
        print(str(ex))
        error = str(ex)
    return error


def manual_add_update(validated_data, user):
    error = None
    try:
        error = "Stocks should not be empty"\
            if len(validated_data['stocks']) == 0\
            else None

        instance = acquisition.header.get(validated_data.id)

        instance.acquired_date = validated_data.get('acquired_date',
                                                    instance.acquired_date)
        instance.po_no = validated_data.get('po_no', instance.po_no)
        instance.inv_no = validated_data.get('inv_no', instance.inv_no)

        instance.save()

        for stock_data in validated_data['stocks']:

            for acqItem in stock_data['acquisitions_items']:

                dbItem = acquisition.item.get(acqItem['id'])

                dbItem.currency = acqItem['currency']

                dbItem.price = acqItem['price']

                dbItem.save()

    except Exception as ex:
        print(f"##ERROR##: manual_add_update:")
        print(str(ex))
        error = str(ex)
    return error
