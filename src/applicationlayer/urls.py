from django.urls import path, include, re_path
from rest_framework import routers
from . import views

from src.applicationlayer.maintenance import views as views_maintenance

from src.applicationlayer.inventory import views as views_inventory

from src.applicationlayer.requisition import views as views_requisition
from src.applicationlayer.purchase_request import views as views_purchase_request
from src.applicationlayer.acquisition import views as views_acquisition
from src.applicationlayer.stock_take import views as views_stock_take

from src.applicationlayer.checkout import views as views_checkout
from src.applicationlayer.checkout_return import views as views_checkout_return

router = routers.DefaultRouter()

router.register(r'Country', views.CountryViewSet, base_name='Country')
router.register(r'City', views.CityViewSet, base_name='City')
router.register(r'Address', views.AddressViewSet, base_name='Address')
router.register(r'ContactType', views.ContactTypeViewSet, base_name='ContactType')
router.register(r'ContactDetail', views.ContactDetailViewSet, base_name='ContactDetail')
router.register(r'Company', views.CompanyViewSet, base_name='Company')
router.register(r'Department', views.DepartmentViewSet, base_name='Department')
router.register(r'Position', views.PositionViewSet, base_name='Position')
router.register(r'User', views.UserViewSet, base_name='User')
router.register(r'UserImage', views.UserImageViewSet, base_name='UserImage')
router.register(r'Attachment', views.AttachmentViewSet, base_name='Attachment')
router.register(r'Announcement', views.AnnouncementViewSet, base_name='Announcement')
router.register(r'ItemCondition', views.ItemConditionViewSet, base_name='ItemCondition')
router.register(r'UnitMaterial', views.UnitMaterialViewSet, base_name='UnitMaterial')
router.register(r'Category', views.CategoryViewSet, base_name='Category')
router.register(r'Brand', views.BrandViewSet, base_name='Brand')
router.register(r'ProductType', views.ProductTypeViewSet, base_name='ProductType')
router.register(r'Currency', views.CurrencyViewSet, base_name='Currency')

router.register(r'Warehouse', views_maintenance.WarehouseViewSet, base_name='Warehouse')
router.register(r'Supplier', views_maintenance.SupplierViewSet, base_name='Supplier')
router.register(r'Client', views_maintenance.ClientViewSet, base_name='Client')
router.register(r'Project', views_maintenance.ProjectViewSet, base_name='Project')
router.register(r'ProjectLocation', views_maintenance.ProjectLocationViewSet, base_name='ProjectLocation')

router.register(r'Product', views_inventory.ProductViewSet, base_name='Product')
router.register(r'ProductImage', views_inventory.ProductImageViewSet, base_name='ProductImage')
router.register(r'ProductUnit', views_inventory.ProductUnitViewSet, base_name='ProductUnit')

router.register(r'Requisition', views_requisition.RequisitionViewSet, base_name='Requisition')
router.register(r'RequisitionProduct', views_requisition.RequisitionProductViewSet, base_name='RequisitionProduct')
router.register(r'RequisitionForceClose', views_requisition.RequisitionForceCloseViewSet, base_name='RequisitionForceClose')
router.register(r'RequisitionApprove', views_requisition.RequisitionApproveViewSet, base_name='RequisitionApprove')
router.register(r'RequisitionApproveItem', views_requisition.RequisitionApproveItemViewSet, base_name='RequisitionApproveItem')
router.register(r'RequisitionReject', views_requisition.RequisitionRejectViewSet, base_name='RequisitionReject')
router.register(r'RequisitionRejectItem', views_requisition.RequisitionRejectItemViewSet, base_name='RequisitionRejectItem')

router.register(r'PurchaseRequest', views_purchase_request.PurchaseRequestViewSet, base_name='PurchaseRequest')
router.register(r'PurchaseRequestItem', views_purchase_request.PurchaseRequestItemViewSet, base_name='PurchaseRequestItem')
router.register(r'PurchaseRequestApprove', views_purchase_request.PurchaseRequestApproveViewSet, base_name='PurchaseRequestApprove')
router.register(r'PurchaseRequestApproveItem', views_purchase_request.PurchaseRequestApproveItemViewSet, base_name='PurchaseRequestApproveItem')
router.register(r'PurchaseRequestReject', views_purchase_request.PurchaseRequestRejectViewSet, base_name='PurchaseRequestReject')
router.register(r'PurchaseRequestRejectItem', views_purchase_request.PurchaseRequestRejectItemViewSet, base_name='PurchaseRequestRejectItem')

router.register(r'Acquisition', views_acquisition.AcquisitionViewSet, base_name='Acquisition')
router.register(r'AcquisitionItem', views_acquisition.AcquisitionItemViewSet, base_name='AcquisitionItem')
router.register(r'Stock', views_acquisition.StockViewSet, base_name='Stock')
router.register(r'StockItem', views_acquisition.StockItemViewSet, base_name='StockItem')

router.register(r'StockTake', views_stock_take.StockTakeViewSet, base_name='StockTake')
router.register(r'StockTakeItem', views_stock_take.StockTakeItemViewSet, base_name='StockTakeItem')
router.register(r'StockTakeItemCount', views_stock_take.StockTakeItemCountViewSet, base_name='StockTakeItemCount')

router.register(r'Checkout', views_checkout.CheckoutViewSet, base_name='Checkout')
router.register(r'CheckoutItem', views_checkout.CheckoutItemViewSet, base_name='CheckoutItem')
router.register(r'CheckoutErrorReturn', views_checkout_return.CheckoutErrorReturnViewSet, base_name='CheckoutErrorReturn')
router.register(r'CheckoutErrorReturnItem', views_checkout_return.CheckoutErrorReturnItemViewSet, base_name='CheckoutErrorReturnItem')
router.register(r'CheckoutReturn', views_checkout_return.CheckoutReturnViewSet, base_name='CheckoutReturn')
router.register(r'CheckoutReturnItem', views_checkout_return.CheckoutReturnItemViewSet, base_name='CheckoutReturnItem')

router.register(r'AssetGroup', views.AssetGroupViewSet, base_name='AssetGroup')
router.register(r'AssetType', views.AssetTypeViewSet, base_name='AssetType')
router.register(r'AssetType', views.AssetViewSet, base_name='AssetType')
router.register(r'AssetStocks', views.AssetStocksViewSet, base_name='AssetStocks')

router.register(r'AuthToken', views.AuthTokenViewSet, base_name='AuthToken')

# CUSTOM
router.register(r'inventory', views_inventory.InventoryViewSet, base_name='Inventory')
router.register(r'acquisition', views_acquisition.ManualAddViewSet, base_name='Manual Add')
router.register(r'requisition', views_requisition.StockRequisitionViewSet, base_name='Stock Requisition')
router.register(r'checkout-requisition', views_checkout.CheckoutRequisitionViewSet, base_name='Checkout Requisition')
router.register(r'checkout-return', views_checkout_return.CheckoutReturnStocksViewSet, base_name='Checkout Return')

urlpatterns = (
    path('', include(router.urls)),
)
