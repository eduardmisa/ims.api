import os
from django.core.mail import send_mail

from entities import models

from api.settings import EMAIL_TEMPLATES_ROOT, SR_APPROVE, SR_REJECT, EMAIL_DEFAULT_SENDER, FRONT_END_URL

from datalayer import productDL


def stock_requisition_new(sr_id,
                          sr_no,
                          requestor,
                          requested_on,
                          project,
                          remarks,
                          receiver,
                          requested_items=[],
                          purchase_items=[],
                          required_date=None):

    F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'stock-requisition.html'), 'r')

    FC = F.read()

    FC = FC.replace('[SR No]', sr_no)
    FC = FC.replace('[Requestor]', requestor)
    FC = FC.replace('[Requested On]', requested_on)
    FC = FC.replace('[Project]', project)
    FC = FC.replace('[Remarks]', remarks)

    # PRINT SR ITEMS
    if requested_items and len(requested_items) > 0:

      appendedList = ''

      for item in requested_items :

        prodNo = str(item.product.product_no)
        prodName = str(item.product.name)
        brand = str(item.product.brand.description)
        qty = str(item.quantity)
        unit = str(item.product.unitmaterial.description)

        appendedList += '\t\t\t\t \
          <tr bgcolor="#FFFFFF"> \
            <td>' + prodNo + '</td> \
            <td>' + prodName + '</td> \
            <td>' + brand + '</td> \
            <td>' + qty + '</td> \
            <td>' + unit + '</td> \
          </tr>'

      FC = FC.replace('<tr bgcolor="#FFFFFF" id="sr">\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                            </tr>', appendedList)

    # PRINT PR ITEMS
    if purchase_items and len(purchase_items) > 0 :

      FC = FC.replace('[Required Date]', required_date)

      appendedList = ''

      for item in purchase_items :

        prodNo = str(item.product.product_no)
        prodName = str(item.product.name)
        brand = str(item.product.brand.description)
        qty = str(item.quantity)
        unit = str(item.product.unitmaterial.description)

        appendedList += '\t\t\t\t \
          <tr bgcolor="#FFFFFF"> \
            <td>' + prodNo + '</td> \
            <td>' + prodName + '</td> \
            <td>' + brand + '</td> \
            <td>' + qty + '</td> \
            <td>' + unit + '</td> \
          </tr>'

      FC = FC.replace('<tr bgcolor="#FFFFFF" id="pr">\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                            </tr>', appendedList)

    urlApprove = FRONT_END_URL + SR_APPROVE + '?sr=' + sr_id
    urlReject = FRONT_END_URL + SR_REJECT + '?sr=' + sr_id

    FC =FC.replace('ApproveToken', urlApprove)
    FC =FC.replace('RejectToken', urlReject)


    # TODO: GET APPROVERS HERE *TEST THIS*

    # approver_emails = []

    # roles_receives = ['Receive Stock Requisition Authorization Email', 'Allow Purchase Requests Authorisation', 'Receive Stock Request Email', 'Receive Purchase Request Email']

    # qry = models.User.objects.filter(user_roles__role__code__in=roles_receives).values('email').distinct()

    # for item in qry :
    #   approver_emails.append(item['email'])

    # if len(approver_emails) > 0:

    #   send_mail(
    #     subject='OB IMS: '+ sr_no +' by ' + requestor,
    #     message='',
    #     from_email=EMAIL_DEFAULT_SENDER,
    #     recipient_list=approver_emails,
    #     html_message=FC
    #   )

    send_mail(
      subject='OB IMS: '+ sr_no +' by ' + requestor,
      message='',
      from_email=EMAIL_DEFAULT_SENDER,
      recipient_list=[receiver,],
      html_message=FC
    )


def stock_requisition_approve(sr_no, requestor, requestor_email, requested_on, requestor_contact, project, remarks, approved_by, approved_date, requested_items = [], purchase_items = [], required_date = None) :

  F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'stock-requisition-approved.html'), 'r')

  FC = F.read()

  FC = FC.replace('[SR No]', sr_no)
  FC = FC.replace('[Requestor]', requestor)
  FC = FC.replace('[Requested On]', requested_on)
  FC = FC.replace('[Project]', project)
  FC = FC.replace('[Remarks]', remarks)

  FC = FC.replace('[Approved By]', approved_by)
  FC = FC.replace('[Approved On]', approved_date)

  # PRINT SR ITEMS
  if len(requested_items) > 0 :

    appendedList = ''

    for item in requested_items :

      prodNo = item.product.product_no
      prodName = item.product.name
      brand = item.product.brand.description
      qty = str(item.quantity)
      unit = item.product.unitmaterial.description

      appendedList += '\t\t\t\t \
        <tr bgcolor="#FFFFFF"> \
          <td>' + prodNo + '</td> \
          <td>' + prodName + '</td> \
          <td>' + brand + '</td> \
          <td>' + qty + '</td> \
          <td>' + unit + '</td> \
        </tr>'

    FC = FC.replace('<tr bgcolor="#FFFFFF" id="sr">\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                            </tr>', appendedList)

  # PRINT PR ITEMS
  if len(purchase_items) > 0 :

    FC = FC.replace('[Required Date]', required_date)

    appendedList = ''

    for item in purchase_items :

      prodNo = item.product.product_no
      prodName = item.product.name
      brand = item.product.brand.description
      qty = str(item.quantity)
      unit = item.product.unitmaterial.description

      appendedList += '\t\t\t\t \
        <tr bgcolor="#FFFFFF"> \
          <td>' + prodNo + '</td> \
          <td>' + prodName + '</td> \
          <td>' + brand + '</td> \
          <td>' + qty + '</td> \
          <td>' + unit + '</td> \
        </tr>'

    FC = FC.replace('<tr bgcolor="#FFFFFF" id="pr">\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                            </tr>', appendedList)


  # TODO: GET APPROVERS HERE *TEST THIS*

  approver_emails = []

  roles_receives = ['Receive Stock Requisition Authorization Email', 'Allow Purchase Requests Authorisation', 'Receive Stock Request Email', 'Receive Purchase Request Email']

  qry = models.User.objects.filter(user_roles__role__code__in=roles_receives).values('email').distinct()

  for item in qry :
    approver_emails.append(item['email'])

  approver_emails.append(requestor_email)

  if len(approver_emails) > 0:

    send_mail(
      subject='OB IMS Stock Request: '+ sr_no,
      message='',
      from_email=EMAIL_DEFAULT_SENDER,
      recipient_list=approver_emails,
      html_message=FC
    )







def stock_requisition_reject(sr_no, requestor, requestor_email, requested_on, project, remarks, rejected_reason, rejected_by, rejected_date, requested_items = [], purchase_items = [], required_date = None) :

  F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'stock-requisition-rejected.html'), 'r')

  FC = F.read()

  FC = FC.replace('[SR No]', sr_no)
  FC = FC.replace('[Requestor]', requestor)
  FC = FC.replace('[Requested On]', requested_on)
  FC = FC.replace('[Project]', project)
  FC = FC.replace('[Remarks]', remarks)

  FC = FC.replace('[Rejected By]', rejected_by)
  FC = FC.replace('[Rejected On]', rejected_date)
  FC = FC.replace('[Rejected Reason]', rejected_reason)

  # PRINT SR ITEMS
  if len(requested_items) > 0 :

    appendedList = ''

    for item in requested_items :

      prodNo = item.product.product_no
      prodName = item.product.name
      brand = item.product.brand.description
      qty = str(item.quantity)
      unit = item.product.unitmaterial.description

      appendedList += '\t\t\t\t \
        <tr bgcolor="#FFFFFF"> \
          <td>' + prodNo + '</td> \
          <td>' + prodName + '</td> \
          <td>' + brand + '</td> \
          <td>' + qty + '</td> \
          <td>' + unit + '</td> \
        </tr>'

    FC = FC.replace('<tr bgcolor="#FFFFFF" id="sr">\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                            </tr>', appendedList)

  # PRINT PR ITEMS
  if len(purchase_items) > 0 :

    FC = FC.replace('[Required Date]', required_date)

    appendedList = ''

    for item in purchase_items :

      prodNo = item.product.product_no
      prodName = item.product.name
      brand = item.product.brand.description
      qty = str(item.quantity)
      unit = item.product.unitmaterial.description

      appendedList += '\t\t\t\t \
        <tr bgcolor="#FFFFFF"> \
          <td>' + prodNo + '</td> \
          <td>' + prodName + '</td> \
          <td>' + brand + '</td> \
          <td>' + qty + '</td> \
          <td>' + unit + '</td> \
        </tr>'

    FC = FC.replace('<tr bgcolor="#FFFFFF" id="pr">\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                                <td>&nbsp;</td>\n                            </tr>', appendedList)


  # TODO: GET APPROVERS HERE *TEST THIS*

  approver_emails = []

  # roles_receives = ['Receive Stock Requisition Authorization Email', 'Allow Purchase Requests Authorisation', 'Receive Stock Request Email', 'Receive Purchase Request Email']

  # qry = models.User.objects.filter(user_roles__role__code__in=roles_receives).values('email').distinct()

  # for item in qry :
  #   approver_emails.append(item['email'])

  approver_emails.append(requestor_email)

  if len(approver_emails) > 0:

    send_mail(
      subject='OB IMS: '+ sr_no +' by ' + requestor,
      message='',
      from_email=EMAIL_DEFAULT_SENDER,
      recipient_list=approver_emails,
      html_message=FC
    )


def account_created(username, password, receiver) :

  F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'account-created.html'), 'r')

  FC = F.read()

  FC = FC.replace('[Username]', username)
  FC = FC.replace('[Password]', password)
  FC = FC.replace('[URL]', FRONT_END_URL)

  send_mail(
    subject='OB IMS: Welcome!',
    message='',
    from_email=EMAIL_DEFAULT_SENDER,
    recipient_list=[receiver,],
    html_message=FC
  )



def account_password_reset(username, password, receiver, date_reset) :

  F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'reset-password.html'), 'r')

  FC = F.read()

  FC = FC.replace('[Username]', username)
  FC = FC.replace('[Password]', password)

  FC = FC.replace('[Reset Datetime]', date_reset)

  send_mail(
    subject='OB IMS: Password Reset!',
    message='',
    from_email=EMAIL_DEFAULT_SENDER,
    recipient_list=[receiver,],
    html_message=FC
  )





def forgot_password(reset_code, url, receiver) :

  F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'forgot-password.html'), 'r')

  FC = F.read()

  FC = FC.replace('[reset_code]', reset_code)
  FC = FC.replace('[URL]', url)

  send_mail(
    subject='OB IMS: Reset Password',
    message='',
    from_email=EMAIL_DEFAULT_SENDER,
    recipient_list=[receiver,],
    html_message=FC
  )




def password_changed(username, date, receiver) :

  F = open(os.path.join(EMAIL_TEMPLATES_ROOT, 'password-changed.html'), 'r')

  FC = F.read()

  FC = FC.replace('[Username]', username)
  FC = FC.replace('[Datetime]', date)
  FC = FC.replace('[URL]', FRONT_END_URL)

  send_mail(
    subject='OB IMS: Password Changed!',
    message='',
    from_email=EMAIL_DEFAULT_SENDER,
    recipient_list=[receiver,],
    html_message=FC
  )