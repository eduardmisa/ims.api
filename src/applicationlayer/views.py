from rest_framework import viewsets, status, serializers
from django.shortcuts import render
from . import serializers
from rest_framework.response import Response

from src.entities import models

from . import paginators
from . import table_filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from src.businesslayer import maintenance

# Create your views here.


class MasterModelViewSet(viewsets.ModelViewSet):
    """ViewSet for the THIS Table"""
    queryset = models.Department.objects.all()
    filter_backends = (DjangoFilterBackend,)
    pagination_class = paginators.SimplePageNumberPagination

    def get_queryset(self):
        return self.model.objects.order_by('id').all()


"""
********************************
* LOCATION / CONTACT / COMPANY *
********************************
"""


class CountryViewSet(MasterModelViewSet):
    """ViewSet for the Country Table"""
    model = models.Country
    serializer_class = serializers.CountrySerializer
    # filterset_class = table_filters.CountryFilter


class CityViewSet(MasterModelViewSet):
    """ViewSet for the City Table"""
    model = models.City
    serializer_class = serializers.CitySerializer
    # filterset_class = table_filters.CityFilter


class AddressViewSet(MasterModelViewSet):
    """ViewSet for the Address Table"""
    model = models.Address
    serializer_class = serializers.AddressSerializer
    # filterset_class = table_filters.AddressFilter


class ContactTypeViewSet(MasterModelViewSet):
    """ViewSet for the ContactType Table"""
    model = models.ContactType
    serializer_class = serializers.ContactTypeSerializer
    # filterset_class = table_filters.ContactTypeFilter


class ContactDetailViewSet(MasterModelViewSet):
    """ViewSet for the ContactDetail Table"""
    model = models.ContactDetail
    serializer_class = serializers.ContactDetailSerializer
    # filterset_class = table_filters.ContactDetailFilter


class CompanyViewSet(MasterModelViewSet):
    """ViewSet for the Company Table"""
    model = models.Company
    serializer_class = serializers.CompanySerializer
    # filterset_class = table_filters.CompanyFilter


class DepartmentViewSet(MasterModelViewSet):
    """ViewSet for the Department Table"""
    model = models.Department
    serializer_class = serializers.DepartmentSerializer
    # filterset_class = table_filters.DepartmentFilter


class PositionViewSet(MasterModelViewSet):
    """ViewSet for the Position Table"""
    model = models.Position
    serializer_class = serializers.PositionSerializer
    # filterset_class = table_filters.PositionFilter


"""
**************************
* USER / ROLE PERMISSION *
**************************
"""


class UserViewSet(MasterModelViewSet):
    """ViewSet for the User Table"""
    model = models.User
    serializer_class = serializers.UserSerializer
    # filterset_class = table_filters.UserFilter


class UserImageViewSet(MasterModelViewSet):
    """ViewSet for the UserImage Table"""
    model = models.UserImage
    serializer_class = serializers.UserImageSerializer
    # filterset_class = table_filters.UserImageFilter


class AttachmentViewSet(MasterModelViewSet):
    """ViewSet for the Attachment Table"""
    model = models.Attachment
    serializer_class = serializers.AttachmentSerializer
    # filterset_class = table_filters.AttachmentFilter


"""
***************
* MAINTENANCE *
***************
"""


class AnnouncementViewSet(MasterModelViewSet):
    """ViewSet for the Announcement Table"""
    model = models.Announcement
    serializer_class = serializers.AnnouncementSerializer
    # filterset_class = table_filters.AnnouncementFilter


class ItemConditionViewSet(MasterModelViewSet):
    """ViewSet for the ItemCondition Table"""
    model = models.ItemCondition
    serializer_class = serializers.ItemConditionSerializer
    # filterset_class = table_filters.ItemConditionFilter


class UnitMaterialViewSet(MasterModelViewSet):
    """ViewSet for the UnitMaterial Table"""
    model = models.UnitMaterial
    serializer_class = serializers.UnitMaterialSerializer
    # filterset_class = table_filters.UnitMaterialFilter


class CategoryViewSet(MasterModelViewSet):
    """ViewSet for the Category Table"""
    model = models.Category
    serializer_class = serializers.CategorySerializer
    # filterset_class = table_filters.CategoryFilter


class BrandViewSet(MasterModelViewSet):
    """ViewSet for the Brand Table"""
    model = models.Brand
    serializer_class = serializers.BrandSerializer
    # filterset_class = table_filters.BrandFilter


class ProductTypeViewSet(MasterModelViewSet):
    """ViewSet for the ProductType Table"""
    model = models.ProductType
    serializer_class = serializers.ProductTypeSerializer
    # filterset_class = table_filters.ProductTypeFilter


class CurrencyViewSet(MasterModelViewSet):
    """ViewSet for the Currency Table"""
    model = models.Currency
    serializer_class = serializers.CurrencySerializer
    # filterset_class = table_filters.CurrencyFilter


"""
*********************
* MASTER RELATIONAL *
*********************
"""


# class WarehouseViewSet(MasterModelViewSet):
#     """ViewSet for the Warehouse Table"""
#     model = models.Warehouse
#     serializer_class = serializers.WarehouseSerializer
#     # filterset_class = table_filters.WarehouseFilter


# class SupplierViewSet(MasterModelViewSet):
#     """ViewSet for the Supplier Table"""
#     model = models.Supplier
#     serializer_class = serializers.SupplierSerializer
#     # filterset_class = table_filters.SupplierFilter

#     @action(detail=True, methods=['get'], url_path='stocks')
#     def stocks(self, request, pk=None):
#         self.serializer_class = serializers.SupplierStocksSerializer
#         self.queryset = maintenance.supplier_stocks(pk)
#         # self.queryset = QuerySetHelper.Sort(self.queryset,RequestHelper.GetParamsValue(self.request, 'sort-field', str), RequestHelper.GetParamsValue(self.request, 'sort-order', str))
#         return super(SuppliersViewSet, self).list(request)


# class ClientViewSet(MasterModelViewSet):
#     """ViewSet for the Client Table"""
#     model = models.Client
#     serializer_class = serializers.ClientSerializer
#     # filterset_class = table_filters.ClientFilter

 
# class ProjectViewSet(MasterModelViewSet):
#     """ViewSet for the Project Table"""
#     model = models.Project
#     serializer_class = serializers.ProjectSerializer
#     filterset_class = table_filters.ProjectFilter

#     def list(self, request, *args, **kwargs):
#         self.serializer_class = serializers.ProjectListSerializer
#         return super(ProjectViewSet, self).list(request)


# class ProjectLocationViewSet(MasterModelViewSet):
#     """ViewSet for the ProjectLocation Table"""
#     model = models.ProjectLocation
#     serializer_class = serializers.ProjectLocationSerializer
#     filterset_class = table_filters.ProjectLocationFilter

#     def list(self, request, *args, **kwargs):
#         self.serializer_class = serializers.ProjectLocationListSerializer
#         return super(ProjectLocationViewSet, self).list(request)


# ********************* TRANSACTION TABLES *********************


"""
************
* PRODUCTS *
************
"""


# class ProductViewSet(MasterModelViewSet):
#     """ViewSet for the Product Table"""
#     model = models.Product
#     # filterset_class = table_filters.ProductFilter


# class ProductImageViewSet(MasterModelViewSet):
#     """ViewSet for the ProductImage Table"""
#     model = models.ProductImage
#     # filterset_class = table_filters.ProductImageFilter


# class ProductUnitViewSet(MasterModelViewSet):
#     """ViewSet for the ProductImage Table"""
#     model = models.ProductUnit
#     # filterset_class = table_filters.ProductImageFilter


"""
*********************
* STOCK REQUISITION *
*********************
"""


# class RequisitionViewSet(MasterModelViewSet):
#     """ViewSet for the Requisition Table"""
#     model = models.Requisition
#     # filterset_class = table_filters.RequisitionFilter


# class RequisitionProductViewSet(MasterModelViewSet):
#     """ViewSet for the RequisitionProduct Table"""
#     model = models.RequisitionProduct
#     # filterset_class = table_filters.RequisitionProductFilter


# class RequisitionForceCloseViewSet(MasterModelViewSet):
#     """ViewSet for the RequisitionForceClose Table"""
#     model = models.RequisitionForceClose
#     # filterset_class = table_filters.RequisitionForceCloseFilter


# class RequisitionApproveViewSet(MasterModelViewSet):
#     """ViewSet for the RequisitionApprove Table"""
#     model = models.RequisitionApprove
#     # filterset_class = table_filters.RequisitionApproveFilter


# class RequisitionApproveItemViewSet(MasterModelViewSet):
#     """ViewSet for the RequisitionApproveItem Table"""
#     model = models.RequisitionApproveItem
#     # filterset_class = table_filters.RequisitionApproveItemFilter


# class RequisitionRejectViewSet(MasterModelViewSet):
#     """ViewSet for the RequisitionReject Table"""
#     model = models.RequisitionReject
#     # filterset_class = table_filters.RequisitionRejectFilter


# class RequisitionRejectItemViewSet(MasterModelViewSet):
#     """ViewSet for the RequisitionRejectItem Table"""
#     model = models.RequisitionRejectItem
#     # filterset_class = table_filters.RequisitionRejectItemFilter


"""
********************
* PURCHASE REQUEST *
********************
"""


# class PurchaseRequestViewSet(MasterModelViewSet):
#     """ViewSet for the PurchaseRequest Table"""
#     model = models.PurchaseRequest
#     # filterset_class = table_filters.PurchaseRequestFilter


# class PurchaseRequestItemViewSet(MasterModelViewSet):
#     """ViewSet for the PurchaseRequestItem Table"""
#     model = models.PurchaseRequestItem
#     # filterset_class = table_filters.PurchaseRequestItemFilter


# class PurchaseRequestApproveViewSet(MasterModelViewSet):
#     """ViewSet for the PurchaseRequestApprove Table"""
#     model = models.PurchaseRequestApprove
#     # filterset_class = table_filters.PurchaseRequestApproveFilter


# class PurchaseRequestApproveItemViewSet(MasterModelViewSet):
#     """ViewSet for the PurchaseRequestApproveItem Table"""
#     model = models.PurchaseRequestApproveItem
#     # filterset_class = table_filters.PurchaseRequestApproveItemFilter


# class PurchaseRequestRejectViewSet(MasterModelViewSet):
#     """ViewSet for the PurchaseRequestReject Table"""
#     model = models.PurchaseRequestReject
#     # filterset_class = table_filters.PurchaseRequestRejectFilter


# class PurchaseRequestRejectItemViewSet(MasterModelViewSet):
#     """ViewSet for the PurchaseRequestRejectItem Table"""
#     model = models.PurchaseRequestRejectItem
#     # filterset_class = table_filters.PurchaseRequestRejectItemFilter


"""
***************
* ACQUISITION *
***************
"""


# class AcquisitionViewSet(MasterModelViewSet):
#     """ViewSet for the Acquisition Table"""
#     model = models.Acquisition
#     # filterset_class = table_filters.AcquisitionFilter


# class AcquisitionItemViewSet(MasterModelViewSet):
#     """ViewSet for the AcquisitionItem Table"""
#     model = models.AcquisitionItem
#     # filterset_class = table_filters.AcquisitionItemFilter


# class StockViewSet(MasterModelViewSet):
#     """ViewSet for the Stock Table"""
#     model = models.Stock
#     # filterset_class = table_filters.StockFilter


# class StockItemViewSet(MasterModelViewSet):
#     """ViewSet for the StockItem Table"""
#     model = models.StockItem
#     # filterset_class = table_filters.StockItemFilter


"""
***************
* STOCK TAKES *
***************
"""


# class StockTakeViewSet(MasterModelViewSet):
#     """ViewSet for the StockTake Table"""
#     model = models.StockTake
#     # filterset_class = table_filters.StockTakeFilter


# class StockTakeItemViewSet(MasterModelViewSet):
#     """ViewSet for the StockTakeItem Table"""
#     model = models.StockTakeItem
#     # filterset_class = table_filters.StockTakeItemFilter


# class StockTakeItemCountViewSet(MasterModelViewSet):
#     """ViewSet for the StockTakeItemCount Table"""
#     model = models.StockTakeItemCount
#     # filterset_class = table_filters.StockTakeItemCountFilter


"""
*************
* CHECKOUTS *
*************
"""


# class CheckoutViewSet(MasterModelViewSet):
#     """ViewSet for the Checkout Table"""
#     model = models.Checkout
#     # filterset_class = table_filters.CheckoutFilter


# class CheckoutItemViewSet(MasterModelViewSet):
#     """ViewSet for the CheckoutItem Table"""
#     model = models.CheckoutItem
#     # filterset_class = table_filters.CheckoutItemFilter


# class CheckoutErrorReturnViewSet(MasterModelViewSet):
#     """ViewSet for the CheckoutErrorReturn Table"""
#     model = models.CheckoutErrorReturn
#     # filterset_class = table_filters.CheckoutErrorReturnFilter


# class CheckoutErrorReturnItemViewSet(MasterModelViewSet):
#     """ViewSet for the CheckoutErrorReturnItem Table"""
#     model = models.CheckoutErrorReturnItem
#     # filterset_class = table_filters.CheckoutErrorReturnItemFilter


# class CheckoutReturnViewSet(MasterModelViewSet):
#     """ViewSet for the CheckoutReturn Table"""
#     model = models.CheckoutReturn
#     # filterset_class = table_filters.CheckoutReturnFilter


# class CheckoutReturnItemViewSet(MasterModelViewSet):
#     """ViewSet for the CheckoutReturnItem Table"""
#     model = models.CheckoutReturnItem
#     # filterset_class = table_filters.CheckoutReturnItemFilter


"""
**********
* ASSETS *
**********
"""


class AssetGroupViewSet(MasterModelViewSet):
    """ViewSet for the AssetGroup Table"""
    model = models.AssetGroup
    serializer_class = serializers.AssetGroupSerializer
    # filterset_class = table_filters.AssetGroupFilter


class AssetTypeViewSet(MasterModelViewSet):
    """ViewSet for the AssetType Table"""
    model = models.AssetType
    serializer_class = serializers.AssetTypeSerializer
    # filterset_class = table_filters.AssetTypeFilter


class AssetViewSet(MasterModelViewSet):
    """ViewSet for the Asset Table"""
    model = models.Asset
    serializer_class = serializers.AssetSerializer
    # filterset_class = table_filters.AssetFilter


class AssetStocksViewSet(MasterModelViewSet):
    """ViewSet for the AssetStocks Table"""
    model = models.AssetStocks
    serializer_class = serializers.AssetStocksSerializer
    # filterset_class = table_filters.AssetStocksFilter


"""
**********
* OTHERS *
**********
"""


class AuthTokenViewSet(MasterModelViewSet):
    """ViewSet for the AuthToken Table"""
    model = models.AuthToken
    serializer_class = serializers.AuthTokenSerializer
    # filterset_class = table_filters.AuthTokenFilter
