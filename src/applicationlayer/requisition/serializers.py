from rest_framework import serializers
from src.entities import models
from src.businesslayer import purchase_request


class StocksForRequisitionSerializer(serializers.Serializer):
    product = serializers.IntegerField()
    productIsSerialized = serializers.CharField(max_length=100)
    productNo = serializers.CharField(max_length=100)
    productName = serializers.CharField(max_length=100)

    productBrand = serializers.CharField(max_length=100)
    productCategory = serializers.CharField(max_length=100)
    productUnit = serializers.CharField(max_length=100)

    qty_total = serializers.IntegerField()
    qty_pri = serializers.IntegerField()
    qty_vb = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


"""
**********************
* DETAIL SERIALIZERS *
**********************
"""


class RequisitionProductsDetailSerializer(serializers.ModelSerializer):
    qty_sr = serializers.SerializerMethodField()
    qty_pr = serializers.SerializerMethodField()

    def get_qty_sr(self, data):
        return data.quantity

    def get_qty_pr(self, data):
        valList = list(purchase_request.requested_pr_count(
            data.product_id,
            data.requisition_id))
        value = 0
        for i in valList:
            value += int(i['val'])

        return value

    class Meta:
        model = models.RequisitionProduct
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionHeaderDetailSerializer(serializers.ModelSerializer):
    requisition_products = RequisitionProductsDetailSerializer(many=True)
    purchase_required_date = serializers.DateField(required=False)

    class Meta:
        model = models.Requisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionHeaderDetailLISTSerializer(serializers.ModelSerializer):
    requisition_products = RequisitionProductsDetailSerializer(many=True)
    purchase_required_date = serializers.DateField(required=False)

    class Meta:
        model = models.Requisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
        depth = 1


"""
**********************
* CREATE SERIALIZERS *
**********************
"""


class RequisitionProductsCreateSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(required=False)
    qty_sr = serializers.IntegerField()
    qty_pr = serializers.IntegerField()

    class Meta:
        model = models.RequisitionProduct
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'status', 'requisition']


class RequisitionHeaderCreateSerializer(serializers.ModelSerializer):
    requisition_products = RequisitionProductsCreateSerializer(many=True)
    purchase_required_date = serializers.DateTimeField(required=False)

    class Meta:
        model = models.Requisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'status', 'requisition_no', 'requestor']


class RequisitionSRItemsSerializer(serializers.Serializer):
    row_id = serializers.IntegerField()
    product_no = serializers.CharField(max_length=100)
    product_name = serializers.CharField(max_length=100)
    brand = serializers.CharField(max_length=100)
    category = serializers.CharField(max_length=100)
    req_status = serializers.CharField(max_length=100)
    unit = serializers.CharField(max_length=100)
    location = serializers.CharField(max_length=100)

    qty_sr = serializers.IntegerField()
    qty_c = serializers.IntegerField()
    qty_rc = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


class RequisitionPRItemsSerializer(serializers.Serializer):
    row_id = serializers.IntegerField()
    pr_header_id = serializers.IntegerField()
    product_no = serializers.CharField(max_length=100)
    product_name = serializers.CharField(max_length=100)
    product_brand = serializers.CharField(max_length=100)
    product_category = serializers.CharField(max_length=100)
    product_unit = serializers.CharField(max_length=100)
    pr_item_status = serializers.CharField(max_length=100)
    qty_pr = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


class RequisitionItemsForApprovalSerializer(serializers.Serializer):
    row_id = serializers.IntegerField()
    pr_header_id = serializers.IntegerField(required=False)
    product_no = serializers.CharField(max_length=100)
    product_name = serializers.CharField(max_length=100)
    product_unit = serializers.CharField(max_length=100)
    req_status = serializers.CharField(max_length=100)
    qty_remaining = serializers.IntegerField()

    class Meta:
        fields = ('__all__')

"""
**********************
* REJECT SERIALIZERS *
**********************
"""


# Stock Requisition REJECT
class RequisitionRejectItemsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.RequisitionRejectItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'requisition_reject']


class RequisitionRejectSerializer(serializers.ModelSerializer):
    requisitions_reject_items = RequisitionRejectItemsSerializer(many=True)

    class Meta:
        model = models.RequisitionReject
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'rejected_by', 'rejected_on']


# Purchase Request REJECT
class PurchaseRequestItemRejectSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.PurchaseRequestRejectItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'purchase_request_reject']


class PurchaseRequestRejectSerializer(serializers.ModelSerializer):
    purchase_requests_reject_items =\
         PurchaseRequestItemRejectSerializer(many=True)

    class Meta:
        model = models.PurchaseRequestReject
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'rejected_by', 'rejected_on']


# REJECT
class RequisitionHeadRejectSerializer(serializers.Serializer):
    sr_reject = RequisitionRejectSerializer(many=False, required=False)
    pr_reject = PurchaseRequestRejectSerializer(many=False, required=False)

    class Meta:
        fields = ('__all__')


"""
***********************
* APPROVE SERIALIZERS *
***********************
"""


# Stock Requisition Approve
class RequisitionApproveItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.RequisitionApproveItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'requisition_approve']


class RequisitionApproveSerializer(serializers.ModelSerializer):
    requisitions_approve_items =\
        RequisitionApproveItemSerializer(many=True)

    class Meta:
        model = models.RequisitionApprove
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'approved_by', 'approved_on']


# Purchase Request Approve
class PurchaseRequestApproveItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.PurchaseRequestApproveItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'purchase_request_approve']


class PurchaseRequestApproveSerializer(serializers.ModelSerializer):
    purchase_requests_approve_items =\
        PurchaseRequestApproveItemSerializer(many=True)

    class Meta:
        model = models.PurchaseRequestApprove
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'approved_by', 'approved_on']


# Approve
class RequisitionHeadApproveSerializer(serializers.Serializer):
    sr_approve = RequisitionApproveSerializer(many=False, required=False)
    pr_approve = PurchaseRequestApproveSerializer(many=False, required=False)

    class Meta:
        fields = ('__all__')
