import copy
import json
from django.shortcuts import render
from rest_framework.response import Response
from src.applicationlayer.views import MasterModelViewSet
from src.entities import models

from django.db import transaction
from src.helper import decorators

from rest_framework import viewsets, status
from rest_framework.decorators import action
from src.businesslayer import requisition

from .. import serializers as app_serializers
from . import serializers

from django_filters.rest_framework import DjangoFilterBackend
from src.applicationlayer import paginators
# Create your views here.


class RequisitionViewSet(MasterModelViewSet):
    """ViewSet for the Requisition Table"""
    model = models.Requisition
    serializer_class = app_serializers.RequisitionSerializer
    # filterset_class = table_filters.RequisitionFilter


class RequisitionProductViewSet(MasterModelViewSet):
    """ViewSet for the RequisitionProduct Table"""
    model = models.RequisitionProduct
    serializer_class = app_serializers.RequisitionProductSerializer
    # filterset_class = table_filters.RequisitionProductFilter


class RequisitionForceCloseViewSet(MasterModelViewSet):
    """ViewSet for the RequisitionForceClose Table"""
    model = models.RequisitionForceClose
    serializer_class = app_serializers.RequisitionForceCloseSerializer
    # filterset_class = table_filters.RequisitionForceCloseFilter


class RequisitionApproveViewSet(MasterModelViewSet):
    """ViewSet for the RequisitionApprove Table"""
    model = models.RequisitionApprove
    serializer_class = app_serializers.RequisitionApproveSerializer
    # filterset_class = table_filters.RequisitionApproveFilter


class RequisitionApproveItemViewSet(MasterModelViewSet):
    """ViewSet for the RequisitionApproveItem Table"""
    model = models.RequisitionApproveItem
    serializer_class = app_serializers.RequisitionApproveItemSerializer
    # filterset_class = table_filters.RequisitionApproveItemFilter


class RequisitionRejectViewSet(MasterModelViewSet):
    """ViewSet for the RequisitionReject Table"""
    model = models.RequisitionReject
    serializer_class = app_serializers.RequisitionRejectSerializer
    # filterset_class = table_filters.RequisitionRejectFilter


class RequisitionRejectItemViewSet(MasterModelViewSet):
    """ViewSet for the RequisitionRejectItem Table"""
    model = models.RequisitionRejectItem
    serializer_class = app_serializers.RequisitionRejectItemSerializer
    # filterset_class = table_filters.RequisitionRejectItemFilter


"""
********************************
*********| CUSTOMS |************
********************************
"""


class StockRequisitionViewSet(viewsets.ModelViewSet):
    """ViewSet for the Requisition"""
    queryset = models.Requisition.objects.all()
    serializer_class = serializers.RequisitionHeaderDetailSerializer
    filter_backends = (DjangoFilterBackend,)
    pagination_class = paginators.SimplePageNumberPagination

    @decorators.error_safe
    def list(self, request, *args, **kwargs):
        self.serializer_class = serializers\
            .RequisitionHeaderDetailLISTSerializer

        return super(StockRequisitionViewSet, self).list(request)

    @decorators.error_safe
    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = serializers\
            .RequisitionHeaderDetailLISTSerializer

        return super(StockRequisitionViewSet, self).retrieve(request)

    @decorators.error_safe
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        self.serializer_class = serializers\
            .RequisitionHeaderCreateSerializer

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            error = requisition\
                        .requisition_save(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None or len(error) == 0:
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED,
                                headers=headers)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)

    @action(detail=True,
            methods=['get'],
            url_path='stock-request-items',
            name="Stock Request Items")
    @decorators.error_safe
    def StockRequestItemsView(self, request, pk=None):

        self.queryset = requisition.list_sr_Items(pk)
        self.serializer_class = serializers.RequisitionSRItemsSerializer

        return super(StockRequisitionViewSet, self).list(request)

    @action(detail=True,
            methods=['get'],
            url_path='purchase-request-items',
            name="Purchase Request Items")
    @decorators.error_safe
    def PurchaseRequestItemsView(self, request, pk=None):

        self.queryset = requisition.list_pr_Items(pk)
        self.serializer_class = serializers.RequisitionPRItemsSerializer

        return super(StockRequisitionViewSet, self).list(request)

    @action(detail=True,
            methods=['put'],
            url_path='cancel',
            name="Cancel Requisition")
    @decorators.error_safe
    @transaction.atomic
    def Cancel(self, request, pk=None):
        error = requisition.requisition_cancel(pk, request.user)
        if error is None:
            return Response(data={"detail": "Requistion Canceled"},
                            status=status.HTTP_202_ACCEPTED)
        else:
            raise Exception(error)

    @action(detail=True,
            methods=['put'],
            url_path='force-close',
            name="Force Close Requisition")
    @decorators.error_safe
    @transaction.atomic
    def ForceClose(self, request, pk=None):
        body_unicode = request.body.decode('utf-8')
        body_data = json.loads(body_unicode)

        error = requisition.requisition_force_close(pk,
                                                    body_data['remarks'],
                                                    request.user)
        if error is None:
            return Response(data={"detail": "Requistion Force Closed"},
                            status=status.HTTP_202_ACCEPTED)
        else:
            raise Exception(error)

    @action(detail=True,
            methods=['put'],
            url_path='reject',
            name="Reject Requisition")
    @decorators.error_safe
    @transaction.atomic
    def Reject(self, request, pk=None):
        self.serializer_class = serializers.RequisitionHeadRejectSerializer

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            error = requisition\
                        .requisition_reject(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None:
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED,
                                headers=headers)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)

    @action(detail=True,
            methods=['get'],
            url_path='approve-stock-request-items',
            name=" For Approval Stock Request Items")
    @decorators.error_safe
    def ApproveStockRequestItems(self, request, pk=None):

        self.queryset = requisition.requisition_approve_sr_items(pk)
        self.serializer_class = serializers\
            .RequisitionItemsForApprovalSerializer

        return super(StockRequisitionViewSet, self).list(request)

    @action(detail=True,
            methods=['get'],
            url_path='approve-purchase-request-items',
            name=" For Approval Purchase Request Items")
    @decorators.error_safe
    def ApprovePurchaseRequestItems(self, request, pk=None):

        self.queryset = requisition.requisition_approve_pr_items(pk)
        self.serializer_class = serializers\
            .RequisitionItemsForApprovalSerializer

        return super(StockRequisitionViewSet, self).list(request)

    @action(detail=True,
            methods=['put'],
            url_path='approve',
            name="Approve Requisition")
    @decorators.error_safe
    @transaction.atomic
    def Approve(self, request, pk=None):
        self.serializer_class = serializers.RequisitionHeadApproveSerializer

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            error = requisition\
                        .requisition_approve(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None:
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED,
                                headers=headers)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)

    @action(detail=True,
            methods=['put'],
            url_path='resend-for-approval',
            name="Resend For Approval")
    @decorators.error_safe
    def ResendForApproval(self, request, pk=None):

        self.queryset = requisition.list_pr_Items(pk)
        self.serializer_class = serializers.RequisitionPRItemsSerializer

        return super(StockRequisitionViewSet, self).list(request)

    @action(detail=False,
            methods=['get'],
            url_path='stocks-for-requisition',
            name="Stocks for Requisition")
    @decorators.error_safe
    def StocksForRequisition(self, request):

        self.queryset = requisition.stocks_for_requisition(True)
        self.serializer_class = serializers.StocksForRequisitionSerializer

        # listAll = request.query_params['listAll']

        # if listAll.lower() == 'true':
        #     self.queryset = requisition.stocks_for_requisition(True)
        # else:
        #     self.queryset = requisition.stocks_for_requisition(False)

        return super(StockRequisitionViewSet, self).list(request)
