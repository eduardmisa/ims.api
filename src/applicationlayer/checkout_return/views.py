import copy
import json
from django.shortcuts import render
from rest_framework.response import Response
from src.applicationlayer.views import MasterModelViewSet
from src.entities import models

from django.db import transaction
from src.helper import decorators

from rest_framework import viewsets, status
from rest_framework.decorators import action
from src.businesslayer import checkout, checkout_return

from .. import serializers as app_serializers
from . import serializers

# Create your views here.


class CheckoutErrorReturnViewSet(MasterModelViewSet):
    """ViewSet for the CheckoutErrorReturn Table"""
    model = models.CheckoutErrorReturn
    serializer_class = app_serializers.CheckoutErrorReturnSerializer
    # filterset_class = table_filters.CheckoutErrorReturnFilter


class CheckoutErrorReturnItemViewSet(MasterModelViewSet):
    """ViewSet for the CheckoutErrorReturnItem Table"""
    model = models.CheckoutErrorReturnItem
    serializer_class = app_serializers.CheckoutErrorReturnItemSerializer
    # filterset_class = table_filters.CheckoutErrorReturnItemFilter


class CheckoutReturnViewSet(MasterModelViewSet):
    """ViewSet for the CheckoutReturn Table"""
    model = models.CheckoutReturn
    serializer_class = app_serializers.CheckoutReturnSerializer
    # filterset_class = table_filters.CheckoutReturnFilter


class CheckoutReturnItemViewSet(MasterModelViewSet):
    """ViewSet for the CheckoutReturnItem Table"""
    model = models.CheckoutReturnItem
    serializer_class = app_serializers.CheckoutReturnItemSerializer
    # filterset_class = table_filters.CheckoutReturnItemFilter


"""
********************************
*********| CUSTOMS |************
********************************
"""


class CheckoutReturnStocksViewSet(viewsets.ModelViewSet):
    """ViewSet for the Checkout Requisition"""
    queryset = models.CheckoutReturn.objects.all()
    serializer_class = serializers.CheckoutReturnDetailSerializer

    @decorators.error_safe
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        self.serializer_class = serializers.CheckoutReturnCreateSerializer

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            error = checkout_return\
                        .checkout_return_save(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None:
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED,
                                headers=headers)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)
