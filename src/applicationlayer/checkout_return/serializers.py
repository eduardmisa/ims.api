from rest_framework import serializers
from src.entities import models
from src.businesslayer import purchase_request

"""
**********************
* DETAIL SERIALIZERS *
**********************
"""


class CheckoutReturnItemDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutReturnItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutReturnDetailSerializer(serializers.ModelSerializer):
    checkout_return_items =\
        CheckoutReturnItemDetailSerializer(many=True,
                                           required=True)

    class Meta:
        model = models.CheckoutReturn
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


"""
**********************
* CREATE SERIALIZERS *
**********************
"""


class CheckoutReturnItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutReturnItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'checkout_return']


class CheckoutReturnCreateSerializer(serializers.ModelSerializer):
    checkout_return_items =\
        CheckoutReturnItemCreateSerializer(many=True,
                                           required=True)

    class Meta:
        model = models.CheckoutReturn
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'return_by', 'return_on']
