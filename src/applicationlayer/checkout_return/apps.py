from django.apps import AppConfig


class CheckoutReturnConfig(AppConfig):
    name = 'checkout_return'
