from rest_framework import serializers
from src.entities import models


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.City
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ContactTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ContactType
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ContactDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ContactDetail
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Company
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Department
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Position
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class UserImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserImage
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Attachment
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Announcement
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ItemConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ItemCondition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class UnitMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UnitMaterial
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Brand
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProductTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductType
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Currency
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class WarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Warehouse
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Supplier
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class SupplierStocksSerializer(serializers.ModelSerializer):
    product_no = serializers.CharField(max_length=100)
    product_name = serializers.CharField(max_length=100)
    product_description = serializers.CharField(max_length=100)
    product_brand = serializers.CharField(max_length=100)
    product_category = serializers.CharField(max_length=100)

    class Meta:
        fields = ('__all__')


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProjectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
        depth = 1


class ProjectLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProjectLocation
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProjectLocationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProjectLocation
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
        depth = 1


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Product
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductImage
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProductUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductUnit
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Requisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequisitionProduct
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionForceCloseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequisitionForceClose
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionApproveSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequisitionApprove
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionApproveItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequisitionApproveItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionRejectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequisitionReject
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionRejectItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RequisitionRejectItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PurchaseRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PurchaseRequest
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PurchaseRequestItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PurchaseRequestItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PurchaseRequestApproveSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PurchaseRequestApprove
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PurchaseRequestApproveItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PurchaseRequestApproveItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PurchaseRequestRejectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PurchaseRequestReject
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class PurchaseRequestRejectItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PurchaseRequestRejectItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Stock
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AcquisitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Acquisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AcquisitionItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AcquisitionItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class StockItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StockItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class StockTakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StockTake
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class StockTakeItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StockTakeItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class StockTakeItemCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StockTakeItemCount
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Checkout
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutErrorReturnSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutErrorReturn
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutErrorReturnItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutErrorReturnItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutReturnSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutReturn
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutReturnItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutReturnItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AssetGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AssetGroup
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AssetTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AssetType
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Asset
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AssetStocksSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AssetStocks
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class AuthTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AuthToken
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
