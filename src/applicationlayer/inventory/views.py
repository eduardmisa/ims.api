from rest_framework import viewsets
from rest_framework.decorators import action
from src.applicationlayer.views import MasterModelViewSet
from src.entities import models
from src.helper import decorators
from src.businesslayer import inventory

from .. import serializers as app_serializers
from . import serializers

from django_filters.rest_framework import DjangoFilterBackend
from src.applicationlayer import paginators

# Create your views here.


class ProductViewSet(MasterModelViewSet):
    """ViewSet for the Product Table"""
    model = models.Product
    serializer_class = app_serializers.ProductSerializer
    # filterset_class = table_filters.ProductFilter


class ProductImageViewSet(MasterModelViewSet):
    """ViewSet for the ProductImage Table"""
    model = models.ProductImage
    serializer_class = app_serializers.ProductImageSerializer
    # filterset_class = table_filters.ProductImageFilter


class ProductUnitViewSet(MasterModelViewSet):
    """ViewSet for the ProductImage Table"""
    model = models.ProductUnit
    serializer_class = app_serializers.ProductUnitSerializer
    # filterset_class = table_filters.ProductImageFilter


"""
********************************
*********| CUSTOMS |************
********************************
"""


class InventoryViewSet(viewsets.ModelViewSet):
    """ViewSet for the Inventory"""
    http_method_names = [u'get']
    queryset = inventory.list_inventory_stocks()
    serializer_class = serializers.InventorySerializer
    filter_backends = (DjangoFilterBackend,)
    pagination_class = paginators.SimplePageNumberPagination

    @decorators.error_safe
    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = serializers.InventoryRetrieveSerializer
        self.queryset = inventory.get_product_info()

        return super(InventoryViewSet, self).retrieve(request)

    @action(detail=True,
            methods=['get'],
            url_path='instore-location',
            name="In Store By Location")
    @decorators.error_safe
    def InstoreLocationView(self, request, pk=None):

        self.queryset = inventory.list_in_store_by_location(pk)
        self.serializer_class = serializers.InventoryInstoreLocationSerializer

        return super(InventoryViewSet, self).list(request)

    @action(detail=True,
            methods=['get'],
            url_path='instore-location-batch',
            name="In Store By Location & Batch")
    @decorators.error_safe
    def InstoreLocationBatchView(self, request, pk=None):

        self.queryset = inventory.list_in_store_by_location_batch(pk)
        self.serializer_class = serializers\
            .InventoryInstoreLocationBatchSerializer

        return super(InventoryViewSet, self).list(request)

    @action(detail=True,
            methods=['get'],
            url_path='instore-location-batch/serialized-items',
            name="In Store By Location & Batch - Serialized Items")
    @decorators.error_safe
    def InstoreLocationBatchView_SerializedItems(self, request, pk=None):
        """ViewSet for the Inventory"""

        self.queryset = inventory\
            .list_in_store_by_location_batch_serialized_items(
                self.request.query_params.get('product_id'),
                self.request.query_params.get('acquisition_id'),
                self.request.query_params.get('warehouse')
            )
        self.serializer_class = serializers\
            .InventoryInstoreLocationBatchSerializedItemsSerializer

        return super(InventoryViewSet, self).list(request)


# TODO: TRANSACTION VIEWS HERE
# ...
# ...

    @action(detail=True,
            methods=['get'],
            url_path='batch-history',
            name="Batch History")
    @decorators.error_safe
    def BatchHistoryView(self, request, pk=None):

        self.queryset = inventory.list_batch_history(pk)
        self.serializer_class = serializers.InventoryBatchHistorySerializer

        return super(InventoryViewSet, self).list(request)

    @action(detail=True,
            methods=['get'],
            url_path='checkout-items',
            name="Checkout Items")
    @decorators.error_safe
    def CheckoutItemsView(self, request, pk=None):

        self.queryset = inventory.list_checkouts(pk)
        self.serializer_class = serializers.InventoryOutItemsSerializer

        return super(InventoryViewSet, self).list(request)

    @action(detail=True,
            methods=['get'],
            url_path='return-items',
            name="Return Items")
    @decorators.error_safe
    def ReturnItemsView(self, request, pk=None):

        self.queryset = inventory.list_returns(pk)
        self.serializer_class = serializers.InventoryReturnItemsSerializer

        return super(InventoryViewSet, self).list(request)
