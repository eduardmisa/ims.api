from rest_framework import serializers
from src.entities import models
from src.businesslayer import inventory


class InventorySerializer(serializers.Serializer):

    # product_images = prod_serializers.ProductCreateImageSerializer(many=True, required=False)

    prod_id = serializers.IntegerField()
    prod_no = serializers.CharField()
    prod_name = serializers.CharField()
    prod_brand = serializers.CharField()
    prod_category = serializers.CharField()
    prod_unit = serializers.CharField()
    qty_is = serializers.IntegerField()
    qty_pri = serializers.IntegerField()
    qty_vb = serializers.IntegerField()
    is_critical = serializers.BooleanField()

    class Meta:
        fields = ('__all__')


class InventoryRetrieveSerializer(serializers.Serializer):

    prod_id = serializers.IntegerField()
    prod_no = serializers.CharField()
    prod_name = serializers.CharField()
    prod_description = serializers.CharField()
    prod_is_serialized = serializers.BooleanField()
    prod_brand = serializers.CharField()
    prod_category = serializers.CharField()
    prod_unit = serializers.CharField()

    prod_quota = serializers.IntegerField()
    total_available_price = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


class InventoryInstoreLocationSerializer(serializers.Serializer):
    current_warehouse = serializers.CharField(max_length=100)
    unit_material = serializers.CharField(max_length=100)
    qty_n = serializers.IntegerField()
    qty_u = serializers.IntegerField()
    qty_t = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


class InventoryInstoreLocationBatchSerializer(serializers.Serializer):
    batch = serializers.CharField(max_length=100)
    current_warehouse = serializers.CharField(max_length=100)
    stock_currency = serializers.CharField(max_length=100)
    acquired_date = serializers.DateTimeField()
    po_no = serializers.CharField(max_length=100)
    inv_no = serializers.CharField(max_length=100)
    unit_material = serializers.CharField(max_length=100)
    supplier = serializers.CharField(max_length=100)
    qty_n = serializers.IntegerField()
    qty_u = serializers.IntegerField()
    qty_t = serializers.IntegerField()
    total_price = serializers.DecimalField(max_digits=15, decimal_places=2)

    class Meta:
        fields = ('__all__')


class InventoryInstoreLocationBatchSerializedItemsSerializer(serializers.Serializer):
    stockId = serializers.IntegerField()
    serial_no = serializers.CharField(max_length=100)
    condition_i = serializers.IntegerField()
    condition_d = serializers.CharField(max_length=100)
    warehouse_i = serializers.IntegerField()
    price_d = serializers.CharField(max_length=100)
    currency_d = serializers.CharField(max_length=100)

    class Meta:
        fields = ('__all__')


# TODO: TRANACTION SERIALIZERS HERE

class InventoryBatchHistorySerializer(serializers.Serializer): 
    acq_id = serializers.IntegerField()
    batch_no = serializers.CharField(max_length=100)
    acquired_date = serializers.DateTimeField()
    currency_d = serializers.CharField(max_length=100)
    po_no = serializers.CharField(max_length=100)
    inv_no = serializers.CharField(max_length=100)
    supplier_d = serializers.CharField(max_length=100)

    total_price = serializers.IntegerField()
    qty = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


class InventoryOutItemsSerializer(serializers.Serializer):
    deployment_date = serializers.DateTimeField()
    location = serializers.CharField(max_length=100)
    project = serializers.CharField(max_length=100)
    requisition_no = serializers.CharField(max_length=100)
    issued_by = serializers.CharField(max_length=100)
    checkout_by = serializers.CharField(max_length=100)

    # serial_numbers = ManageStockOutItemSerialsSerializer(many=True, required=True)
    requestor_remarks = serializers.CharField(max_length=100)
    remarks_collector = serializers.CharField(max_length=100)

    qty_n = serializers.IntegerField()
    qty_u = serializers.IntegerField()
    qty_t = serializers.IntegerField()

    class Meta:
        fields = ('__all__')


class InventoryReturnItemsSerializer(serializers.Serializer):
    chk_rtn_id = serializers.IntegerField()
    requisition_no = serializers.CharField(max_length=100)
    # checkout_no = serializers.CharField(max_length=100)
    return_date = serializers.DateTimeField()
    to_warehouse = serializers.CharField(max_length=100)
    from_project = serializers.CharField(max_length=100)

    return_by_user = serializers.CharField(max_length=100)
    # received_by = serializers.CharField(max_length=100)

    # serial_numbers = ManageStockOutItemSerialsSerializer(many=True, required=True)

    qty_n = serializers.IntegerField()
    qty_u = serializers.IntegerField()
    qty_t = serializers.IntegerField()

    class Meta:
        fields = ('__all__')
