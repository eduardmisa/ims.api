from rest_framework import serializers
from src.entities import models


class WarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Warehouse
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Supplier
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class SupplierStocksSerializer(serializers.Serializer):
    product_no = serializers.CharField(max_length=100)
    product_name = serializers.CharField(max_length=100)
    product_description = serializers.CharField(max_length=100)
    product_brand = serializers.CharField(max_length=100)
    product_category = serializers.CharField(max_length=100)

    class Meta:
        fields = ('__all__')


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ClientListSerializer(ClientSerializer):
    no_of_projects = serializers.IntegerField()

    class Meta:
        model = models.Client
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProjectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
        depth = 1


class ProjectLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProjectLocation
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ProjectLocationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProjectLocation
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
        depth = 1
