import copy
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from django.db import transaction
from src.helper import decorators

from src.applicationlayer.views import MasterModelViewSet
from src.applicationlayer import table_filters
from src.entities import models

# from . import table_filters
from . import serializers
from .. import paginators

from src.businesslayer import maintenance

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
# Create your views here.


class WarehouseViewSet(MasterModelViewSet):
    """ViewSet for the Warehouse Table"""
    model = models.Warehouse
    serializer_class = serializers.WarehouseSerializer
    # filterset_class = table_filters.WarehouseFilter


class SupplierViewSet(viewsets.ModelViewSet):
    """ViewSet for the Supplier Table"""
    queryset = models.Supplier.objects.all()
    filter_backends = (DjangoFilterBackend,)
    pagination_class = paginators.SimplePageNumberPagination

    serializer_class = serializers.SupplierSerializer
    # filterset_class = table_filters.SupplierFilter

    @action(detail=True, methods=['get'], url_path='stocks')
    def stocks(self, request, pk=None):
        self.serializer_class = serializers.SupplierStocksSerializer
        self.queryset = maintenance.supplier_stocks(pk)
        # self.queryset = QuerySetHelper.Sort(self.queryset,RequestHelper.GetParamsValue(self.request, 'sort-field', str), RequestHelper.GetParamsValue(self.request, 'sort-order', str))
        return super(SupplierViewSet, self).list(request)


class ClientViewSet(viewsets.ModelViewSet):
    """ViewSet for the Client Table"""
    queryset = models.Supplier.objects.all()
    filter_backends = (DjangoFilterBackend,)
    pagination_class = paginators.SimplePageNumberPagination

    serializer_class = serializers.ClientSerializer
    # filterset_class = table_filters.ClientFilter

    def list(self, request, *args, **kwargs):
        self.queryset = maintenance.clients_with_project_count()
        self.serializer_class = serializers.ClientListSerializer
        return super(ClientViewSet, self).list(request)


class ProjectViewSet(MasterModelViewSet):
    """ViewSet for the Project Table"""
    model = models.Project
    serializer_class = serializers.ProjectSerializer
    filterset_class = table_filters.ProjectFilter

    def list(self, request, *args, **kwargs):
        self.serializer_class = serializers.ProjectListSerializer
        return super(ProjectViewSet, self).list(request)


class ProjectLocationViewSet(MasterModelViewSet):
    """ViewSet for the ProjectLocation Table"""
    model = models.ProjectLocation
    serializer_class = serializers.ProjectLocationSerializer
    filterset_class = table_filters.ProjectLocationFilter

    def list(self, request, *args, **kwargs):
        self.serializer_class = serializers.ProjectLocationListSerializer
        return super(ProjectLocationViewSet, self).list(request)
