import copy
import json
from django.shortcuts import render
from rest_framework.response import Response
from src.applicationlayer.views import MasterModelViewSet
from src.entities import models

from django.db import transaction
from src.helper import decorators

from rest_framework import viewsets, status
from rest_framework.decorators import action
from src.businesslayer import checkout

from .. import serializers as app_serializers
from . import serializers

# Create your views here.


class CheckoutViewSet(MasterModelViewSet):
    """ViewSet for the Checkout Table"""
    model = models.Checkout
    serializer_class = app_serializers.CheckoutSerializer
    # filterset_class = table_filters.CheckoutFilter


class CheckoutItemViewSet(MasterModelViewSet):
    """ViewSet for the CheckoutItem Table"""
    model = models.CheckoutItem
    serializer_class = app_serializers.CheckoutItemSerializer
    # filterset_class = table_filters.CheckoutItemFilter


"""
********************************
*********| CUSTOMS |************
********************************
"""


class CheckoutRequisitionViewSet(viewsets.ModelViewSet):
    """ViewSet for the Checkout Requisition"""
    queryset = models.Checkout.objects.all()
    serializer_class = serializers.CheckoutDetailSerializer

    @decorators.error_safe
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        self.serializer_class = serializers.CheckoutCreateSerializer

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            error = checkout\
                        .checkout_save(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None:
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED,
                                headers=headers)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)
