from rest_framework import serializers
from src.entities import models
from src.businesslayer import purchase_request

"""
**********************
* DETAIL SERIALIZERS *
**********************
"""


class CheckoutItemStockItemsDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.StockItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutItemDetailSerializer(serializers.ModelSerializer):
    stock_item = CheckoutItemStockItemsDetailSerializer(many=False,
                                                        required=True)

    class Meta:
        model = models.CheckoutItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class CheckoutDetailSerializer(serializers.ModelSerializer):
    checkout_items = CheckoutItemDetailSerializer(many=True,
                                                  required=True)

    class Meta:
        model = models.Checkout
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


"""
**********************
* CREATE SERIALIZERS *
**********************
"""


class CheckoutItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CheckoutItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'checkout']


class CheckoutCreateSerializer(serializers.ModelSerializer):
    checkout_items = CheckoutItemCreateSerializer(many=True,
                                                  required=True)

    class Meta:
        model = models.Checkout
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'checkout_no']
