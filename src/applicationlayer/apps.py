from django.apps import AppConfig


class ApplicationLayerConfig(AppConfig):
    name = 'application_layer'
