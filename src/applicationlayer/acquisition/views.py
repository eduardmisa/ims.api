import copy
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import viewsets, status
from rest_framework.decorators import action
from django.db import transaction
from src.helper import decorators

from src.applicationlayer.views import MasterModelViewSet
from src.entities import models

from src.businesslayer import acquisition

from .. import serializers as app_serializers
from . import serializers


# Create your views here.


class AcquisitionViewSet(MasterModelViewSet):
    """ViewSet for the Acquisition Table"""
    model = models.Acquisition
    serializer_class = app_serializers.AcquisitionSerializer
    # filterset_class = table_filters.AcquisitionFilter


class AcquisitionItemViewSet(MasterModelViewSet):
    """ViewSet for the AcquisitionItem Table"""
    model = models.AcquisitionItem
    serializer_class = app_serializers.AcquisitionItemSerializer
    # filterset_class = table_filters.AcquisitionItemFilter


class StockViewSet(MasterModelViewSet):
    """ViewSet for the Stock Table"""
    model = models.Stock
    serializer_class = app_serializers.StockSerializer
    # filterset_class = table_filters.StockFilter


class StockItemViewSet(MasterModelViewSet):
    """ViewSet for the StockItem Table"""
    model = models.StockItem
    serializer_class = app_serializers.StockItemSerializer
    # filterset_class = table_filters.StockItemFilter


"""
********************************
*********| CUSTOMS |************
********************************
"""


class ManualAddViewSet(viewsets.ModelViewSet):
    """ViewSet for the Inventory"""
    queryset = acquisition.manual_add()
    serializer_class = serializers.ManualAddAcquisitionDetailSerializer

    @decorators.error_safe
    @transaction.atomic
    def create(self, request, *args, **kwargs):
        self.serializer_class = serializers\
            .ManualAddAcquisitionCreateSerializer

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            error = acquisition\
                        .manual_add_save(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None:
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED,
                                headers=headers)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)

    @decorators.error_safe
    @transaction.atomic
    def update(self, request, *args, **kwargs):
        self.serializer_class = serializers\
            .ManualAddAcquisitionUpdateSerializer

        instance = self.get_object()
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=False)
        if serializer.is_valid():
            error = acquisition\
                        .manual_add_update(
                            copy.deepcopy(serializer.validated_data),
                            request.user)
            if error is None:
                return Response(serializer.data)
            else:
                raise Exception(error)
        else:
            serializer.is_valid(raise_exception=True)
