from rest_framework import serializers
from src.entities import models
from src.businesslayer import acquisition

from rest_framework.validators import UniqueValidator


"""
**********************
* DETAIL SERIALIZERS *
**********************
"""


class ManualAddAcquisitionItemDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.AcquisitionItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ManualAddStockDetailSerializer(serializers.ModelSerializer):
    acquisition_items = ManualAddAcquisitionItemDetailSerializer(many=True,
                                                                 required=True)

    class Meta:
        model = models.Stock
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class ManualAddAcquisitionDetailSerializer(serializers.ModelSerializer):
    stocks = serializers.SerializerMethodField()

    def get_stocks(self, data):
        return ManualAddStockDetailSerializer(
                acquisition.manual_add_details(data.id),
                many=True,
                required=True,
                context=self.context).data

    class Meta:
        model = models.Acquisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


"""
**********************
* CREATE SERIALIZERS *
**********************
"""


class ManualAddAcquisitionItemCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.AcquisitionItem
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'stock', 'acquisition']


class ManualAddStockCreateSerializer(serializers.ModelSerializer):
    acquisition_items = ManualAddAcquisitionItemCreateSerializer(many=True,
                                                                 required=True)

    class Meta:
        model = models.Stock
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'stock_no']


class ManualAddAcquisitionCreateSerializer(serializers.ModelSerializer):
    stocks = ManualAddStockCreateSerializer(many=True,
                                            required=True)

    class Meta:
        model = models.Acquisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified',
                            'acquisition_no']


"""
**********************
* UPDATE SERIALIZERS *
**********************
"""


class ManualAddAcquisitionItemUpdateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = models.AcquisitionItem
        fields = ('__all__')


class ManualAddStockUpdateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    acquisition_items =\
        ManualAddAcquisitionItemUpdateSerializer(many=True, required=True)

    class Meta:
        model = models.Stock
        exclude = ('serial_number', 'stock_no')


class ManualAddAcquisitionUpdateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    stocks = ManualAddStockUpdateSerializer(many=True, required=True)

    class Meta:
        model = models.Acquisition
        fields = ('__all__')
