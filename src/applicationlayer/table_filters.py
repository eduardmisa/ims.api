from django_filters import rest_framework as filters
from django.db.models import Count
from src.entities import models
from django.db.models import Q


class CountryFilter(filters.FilterSet):
    search = filters.CharFilter(method='search_bar')

    def search_bar(self, queryset, name, value):
        return queryset.filter(
            Q(supplier_no__icontains=value) |
            Q(company_name__icontains=value) |
            Q(contact_person__icontains=value)
        )

    class Meta:
        model = models.Country
        fields = ('__all__')


class ProjectFilter(filters.FilterSet):
    search = filters.CharFilter(method='search_bar')

    def search_bar(self, queryset, name, value):
        return queryset

    class Meta:
        model = models.Project
        fields = ('__all__')


class ProjectLocationFilter(filters.FilterSet):
    search = filters.CharFilter(method='search_bar')

    def search_bar(self, queryset, name, value):
        return queryset

    class Meta:
        model = models.ProjectLocation
        fields = ('__all__')
