from django.shortcuts import render
from src.applicationlayer.views import MasterModelViewSet
from src.entities import models


from .. import serializers as app_serializers
# Create your views here.


class PurchaseRequestViewSet(MasterModelViewSet):
    """ViewSet for the PurchaseRequest Table"""
    model = models.PurchaseRequest
    serializer_class = app_serializers.PurchaseRequestSerializer
    # filterset_class = table_filters.PurchaseRequestFilter


class PurchaseRequestItemViewSet(MasterModelViewSet):
    """ViewSet for the PurchaseRequestItem Table"""
    model = models.PurchaseRequestItem
    serializer_class = app_serializers.PurchaseRequestItemSerializer
    # filterset_class = table_filters.PurchaseRequestItemFilter


class PurchaseRequestApproveViewSet(MasterModelViewSet):
    """ViewSet for the PurchaseRequestApprove Table"""
    model = models.PurchaseRequestApprove
    serializer_class = app_serializers.PurchaseRequestApproveSerializer
    # filterset_class = table_filters.PurchaseRequestApproveFilter


class PurchaseRequestApproveItemViewSet(MasterModelViewSet):
    """ViewSet for the PurchaseRequestApproveItem Table"""
    model = models.PurchaseRequestApproveItem
    serializer_class = app_serializers.PurchaseRequestApproveItemSerializer
    # filterset_class = table_filters.PurchaseRequestApproveItemFilter


class PurchaseRequestRejectViewSet(MasterModelViewSet):
    """ViewSet for the PurchaseRequestReject Table"""
    model = models.PurchaseRequestReject
    serializer_class = app_serializers.PurchaseRequestRejectSerializer
    # filterset_class = table_filters.PurchaseRequestRejectFilter


class PurchaseRequestRejectItemViewSet(MasterModelViewSet):
    """ViewSet for the PurchaseRequestRejectItem Table"""
    model = models.PurchaseRequestRejectItem
    serializer_class = app_serializers.PurchaseRequestRejectItemSerializer
    # filterset_class = table_filters.PurchaseRequestRejectItemFilter
