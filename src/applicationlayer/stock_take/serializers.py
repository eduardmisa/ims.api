from rest_framework import serializers
from src.entities import models
from src.businesslayer import purchase_request


"""
**********************
* DETAIL SERIALIZERS *
**********************
"""


class RequisitionProductsDetailSerializer(serializers.ModelSerializer):
    qty_sr = serializers.SerializerMethodField()
    qty_pr = serializers.SerializerMethodField()

    def get_qty_sr(self, data):
        return data.quantity

    def get_qty_pr(self, data):
        valList = list(purchase_request.requested_pr_count(
            data.product_id,
            data.requisition_id))
        value = 0
        for i in valList:
            value += int(i['val'])

        return value

    class Meta:
        model = models.RequisitionProduct
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']


class RequisitionHeaderDetailSerializer(serializers.ModelSerializer):
    requisition_products = RequisitionProductsDetailSerializer(many=True)
    purchase_required_date = serializers.DateField(required=False)

    class Meta:
        model = models.Requisition
        fields = '__all__'
        read_only_fields = ['createdby', 'modifiedby', 'created', 'modified']
