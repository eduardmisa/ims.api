from django.apps import AppConfig


class StockTakeConfig(AppConfig):
    name = 'stock_take'
