from django.shortcuts import render
from src.applicationlayer.views import MasterModelViewSet
from src.entities import models

from .. import serializers as app_serializers

# Create your views here.


class StockTakeViewSet(MasterModelViewSet):
    """ViewSet for the StockTake Table"""
    model = models.StockTake
    serializer_class = app_serializers.StockTakeSerializer
    # filterset_class = table_filters.StockTakeFilter


class StockTakeItemViewSet(MasterModelViewSet):
    """ViewSet for the StockTakeItem Table"""
    model = models.StockTakeItem
    serializer_class = app_serializers.StockTakeItemSerializer
    # filterset_class = table_filters.StockTakeItemFilter


class StockTakeItemCountViewSet(MasterModelViewSet):
    """ViewSet for the StockTakeItemCount Table"""
    model = models.StockTakeItemCount
    serializer_class = app_serializers.StockTakeItemCountSerializer
    # filterset_class = table_filters.StockTakeItemCountFilter
