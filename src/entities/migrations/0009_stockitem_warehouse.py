# Generated by Django 2.2 on 2019-05-06 11:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0008_auto_20190506_1113'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockitem',
            name='warehouse',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='stock_items_stock', to='entities.Warehouse'),
            preserve_default=False,
        ),
    ]
