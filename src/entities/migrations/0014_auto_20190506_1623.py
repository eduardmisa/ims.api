# Generated by Django 2.2 on 2019-05-06 16:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0013_auto_20190506_1539'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checkoutitem',
            name='acquisition_item',
        ),
        migrations.AddField(
            model_name='checkoutitem',
            name='stock_items',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='checkouts_items', to='entities.StockItem'),
            preserve_default=False,
        ),
    ]
