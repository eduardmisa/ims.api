# Generated by Django 2.2 on 2019-05-06 10:09

from django.db import migrations, models
import src.entities.enums


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0005_stockitem_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockitem',
            name='status',
            field=models.CharField(choices=[(src.entities.enums.StockEnum('In'), 'In'), (src.entities.enums.StockEnum('Out'), 'Out')], default='In', max_length=20),
        ),
    ]
