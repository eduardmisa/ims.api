# Generated by Django 2.2 on 2019-05-09 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0018_auto_20190508_0111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchaserequest',
            name='status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Partially Approved', 'Partially Approved'), ('Rejected', 'Rejected'), ('Canceled', 'Canceled'), ('Closed', 'Closed'), ('Force Closed', 'Force Closed')], default='Pending', max_length=20),
        ),
        migrations.AlterField(
            model_name='purchaserequestitem',
            name='status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Partially Approved', 'Partially Approved'), ('Rejected', 'Rejected'), ('Canceled', 'Canceled'), ('Closed', 'Closed'), ('Force Closed', 'Force Closed')], default='Pending', max_length=20),
        ),
        migrations.AlterField(
            model_name='requisition',
            name='status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Partially Approved', 'Partially Approved'), ('Rejected', 'Rejected'), ('Canceled', 'Canceled'), ('Closed', 'Closed'), ('Force Closed', 'Force Closed')], default='Pending', max_length=20),
        ),
        migrations.AlterField(
            model_name='requisitionproduct',
            name='status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Approved', 'Approved'), ('Partially Approved', 'Partially Approved'), ('Rejected', 'Rejected'), ('Canceled', 'Canceled'), ('Closed', 'Closed'), ('Force Closed', 'Force Closed')], default='Pending', max_length=20),
        ),
    ]
