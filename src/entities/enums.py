from enum import Enum


class ModelSeriesEnum(Enum):
    USER = "USR"
    CLIENT = "CLI"
    CONSUMABLEPRODUCTS = "CSP"
    PROJECT = "PRJ"
    SERIALIZEITEM = "ITM"
    SERIALIZEPRODUCT = "SRP"
    SUPPLIER = "SUP"
    WAREHOUSE = "WHL"
    ACQUISITION = 'AQN'
    REQUISITION = 'RQN'
    PURCHASEREQUEST = 'PR'
    STOCKTAKE = 'STK'
    CHECKOUT = 'CHK'


class ItemConditionEnum(Enum):
    NEW = "New"
    USED = "Used"


class StockTakeEnum(Enum):
    OPEN = "Open"
    CLOSED = "Closed"


class RequisitionEnum(Enum):
    # ACIVE = "Active"
    # DEACTIVATED = "Deactivated"
    # DELETED = "Deleted"
    # ARCHIVED = "Archived"
    PENDING = "Pending"
    APPROVED = "Approved"
    APPROVED_PARTIAL = "Partially Approved"
    REJECTED = "Rejected"
    REJECTED_PARTIAL = "Partially Rejected"
    APPROVED_REJECTED_PARTIAL = "Partially Approved & Rejected"

    CANCELED = "Canceled"
    CLOSED = "Closed"
    FORCE_CLOSED = "Force Closed"


class StockEnum(Enum):
    IN = "In"
    OUT = "Out"


class CheckoutEnum(Enum):
    DISPATCHED = "Dispatched"
    RETURNED = "Returned"
