from django.db import models
from django.contrib.auth.models import AbstractUser
from .modelcommon import AuditClass
from .UserManager import UserManager
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from datetime import datetime
from . import enums


# ********************* AUTH TABLES *********************


class Country(AuditClass):
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'countries'


class City(AuditClass):
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    country = models.ForeignKey(Country,
                                related_name='cities',
                                on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'cities'


class Address(AuditClass):
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    city = models.ForeignKey(City,
                             related_name='addresses',
                             on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'addresses'


class ContactType(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    regex = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'contact_types'


class ContactDetail(AuditClass):
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    is_primary = models.BooleanField()

    contact_type = models.ForeignKey(ContactType,
                                     related_name='contact_details',
                                     on_delete=models.PROTECT)

    def __str__(self):
        return self.value

    class Meta:
        db_table = 'contact_details'


class Company(AuditClass):
    company_no = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)

    addresses = models.ManyToManyField(Address, related_name='companies')
    contact_details = models.ManyToManyField(ContactDetail,
                                             related_name='companies')

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'companies'


class Department(AuditClass):
    parent = models.ForeignKey('Department',
                               related_name='department_subs',
                               on_delete=models.PROTECT,
                               blank=True, null=True)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    company = models.ForeignKey(Company,
                                related_name='departments',
                                on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'departments'


class Position(AuditClass):
    parent = models.ForeignKey('Position',
                               related_name='position_subs',
                               on_delete=models.PROTECT,
                               blank=True, null=True)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    department = models.ForeignKey(Department,
                                   related_name='possitions',
                                   on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'positions'


class User(AbstractUser):
    nric = models.CharField(blank=True, null=True, max_length=255)
    contact_no = models.CharField(blank=True, null=True, max_length=255)

    objects = UserManager()

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'auth_user'


class UserImage(AuditClass):
    name = models.CharField(blank=True, null=True, max_length=255)
    image = models.ImageField(upload_to='user_images/', null=True,
                              max_length=255)
    is_primary = models.BooleanField(default=True)

    user = models.ForeignKey(User, related_name='user_images',
                             on_delete=models.PROTECT)

    def __str__(self):
        return f"User({self.user.username})-UserImage({self.name})"

    class Meta:
        db_table = 'auth_user_images'


class Attachment(AuditClass):
    code = models.FileField(upload_to='file_attachments/', null=True,
                            max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'attachments'


# ********************* MASTER TABLES *********************


class Announcement(AuditClass):
    title = models.CharField(unique=True, max_length=255)
    message = models.CharField(max_length=255, blank=False, null=True)
    flag = models.CharField(max_length=255)
    visibility = models.BooleanField(default=True)
    published = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'announcements'


class ItemCondition(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'item_conditions'


class UnitMaterial(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'unit_materials'


# TODO: IMPRTANT! Adjusted Unique due to data duplicates
# code, name
class Category(AuditClass):
    parent = models.ForeignKey('Category', related_name='category_subs',
                               on_delete=models.PROTECT,
                               blank=True, null=True)
    code = models.CharField(unique=False, max_length=255)
    name = models.CharField(unique=False, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'categories'


class Brand(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'brands'


class ProductType(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'product_types'


class Currency(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'currencies'


class Warehouse(AuditClass):
    warehouse_no = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)

    address = models.CharField(max_length=255, blank=True, null=True)

    is_stock_visible = models.BooleanField()

    contact_person = models.CharField(max_length=255, blank=True, null=True)
    contact_email = models.EmailField(max_length=255, blank=True, null=True)
    contact_number = models.CharField(max_length=255, blank=True, null=True)
    contact_website = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'warehouses'


class Supplier(AuditClass):
    supplier_no = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)

    address = models.CharField(max_length=255, blank=True, null=True)

    contact_person = models.CharField(max_length=255, blank=True, null=True)
    contact_email = models.EmailField(max_length=255, blank=True, null=True)
    contact_number = models.CharField(max_length=255, blank=True, null=True)
    contact_website = models.URLField(blank=True, null=True)

    remarks = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'suppliers'


class Client(AuditClass):
    client_no = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)

    contact_person = models.CharField(max_length=255, blank=True, null=True)
    contact_email = models.EmailField(max_length=255, blank=True, null=True)
    contact_number = models.CharField(max_length=255, blank=True, null=True)
    contact_website = models.URLField(blank=True, null=True)

    remarks = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'clients'


class Project(AuditClass):
    project_no = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    date_start = models.DateTimeField(blank=True, null=True)
    date_end = models.DateTimeField(blank=True, null=True)

    contact_person = models.CharField(max_length=255, blank=True, null=True)
    contact_email = models.EmailField(max_length=255, blank=True, null=True)
    contact_number = models.CharField(max_length=255, blank=True, null=True)
    contact_website = models.URLField(blank=True, null=True)

    is_visible = models.BooleanField(default=True)

    client = models.ForeignKey(Client,
                               related_name='projects',
                               on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'projects'


# TODO: IMPRTANT! Adjusted Unique due to data duplicates
# name
class ProjectLocation(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=False, max_length=255)
    addresses = models.CharField(max_length=255, blank=True, null=True)

    contact_person = models.CharField(max_length=255, blank=True, null=True)
    contact_email = models.EmailField(max_length=255, blank=True, null=True)
    contact_number = models.CharField(max_length=255, blank=True, null=True)
    contact_website = models.URLField(blank=True, null=True)

    project = models.ForeignKey(Project,
                                related_name='project_locations',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Project({self.project.name})"\
               f"-ProjectLocation({self.name})"

    class Meta:
        db_table = 'project_locations'


# ********************* TRANSACTION TABLES *********************


# TODO: IMPRTANT! Adjusted Unique due to data duplicates
# name
class Product(AuditClass):
    product_no = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=False, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    minimum_quantity = models.IntegerField(blank=True, null=True)
    is_serialized = models.BooleanField(default=True)

    unit_material = models.ForeignKey(UnitMaterial,
                                      related_name='products',
                                      on_delete=models.PROTECT)

    brand = models.ForeignKey(Brand,
                              related_name='products',
                              on_delete=models.PROTECT)
    # product_type = models.ForeignKey(ProductType,
    #                                  related_name='products',
    #                                  on_delete=models.PROTECT)
    category = models.ForeignKey(Category,
                                 related_name='products',
                                 on_delete=models.PROTECT)

    def __str__(self):
        return self.product_no

    class Meta:
        db_table = 'products'


class ProductImage(AuditClass):
    name = models.CharField(blank=True, null=True, max_length=255)
    image = models.ImageField(upload_to='product_images/',
                              null=True, max_length=255)
    is_primary = models.BooleanField(default=True)

    product = models.ForeignKey(Product,
                                related_name='product_images',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Product({self.product.product_no})"\
               f"-ProductImage({self.name})"

    class Meta:
        db_table = 'product_images'


class ProductUnit(AuditClass):
    is_primary = models.BooleanField(default=True)
    unit_material = models.ForeignKey(UnitMaterial,
                                      related_name='product_units',
                                      on_delete=models.PROTECT)
    product = models.ForeignKey(Product,
                                related_name='product_units',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Product({self.product.product_no})"\
               f"-ProductUnit({self.id})"

    class Meta:
        db_table = 'product_units'


class Requisition(AuditClass):
    status = models.CharField(
                        choices=[
                            (tag.value, tag.value) for tag in enums.RequisitionEnum],
                        default=enums.RequisitionEnum.PENDING.value,
                        max_length=50)

    requisition_no = models.CharField(unique=True, max_length=255)

    requestor = models.ForeignKey(User,
                                  related_name='requisitions',
                                  on_delete=models.PROTECT)
    requested_date = models.DateTimeField(blank=True, null=True)

    approver = models.ForeignKey(User,
                                 related_name='requisitions_approver',
                                 on_delete=models.PROTECT,
                                 blank=True, null=True)

    checkout_close = models.BooleanField(default=False)

    approve_as_urgent = models.BooleanField(default=False)

    project = models.ForeignKey(Project,
                                related_name='requisitions',
                                on_delete=models.PROTECT)

    remarks = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.requisition_no

    class Meta:
        db_table = 'requisitions'


class RequisitionProduct(AuditClass):
    status = models.CharField(
                        choices=[
                            (tag.value, tag.value) for tag in enums.RequisitionEnum],
                        default=enums.RequisitionEnum.PENDING.value,
                        max_length=50)

    quantity = models.IntegerField(blank=True, null=True)
    remarks = models.CharField(max_length=255, blank=True, null=True)

    product = models.ForeignKey(Product,
                                related_name='requisition_products',
                                on_delete=models.PROTECT)
    requisition = models.ForeignKey(Requisition,
                                    related_name='requisition_products',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-RequisitionProduct({self.product.name})"

    class Meta:
        db_table = 'requisition_products'


class RequisitionForceClose(AuditClass):
    close_remarks = models.CharField(max_length=255,
                                     blank=True, null=True)
    closed_by = models.ForeignKey(User,
                                  related_name='requisitions_force_close',
                                  on_delete=models.PROTECT)
    closed_on = models.DateTimeField()

    requisition = models.ForeignKey(Requisition,
                                    related_name='requisitions_force_close',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-RequisitionForceClose({self.id})"

    class Meta:
        db_table = 'requisitions_force_close'


class RequisitionApprove(AuditClass):
    remarks = models.CharField(max_length=255, blank=True, null=True)
    approved_by = models.ForeignKey(User,
                                    related_name='requisitions_approve',
                                    on_delete=models.PROTECT)
    approved_on = models.DateTimeField()

    requisition = models.ForeignKey(Requisition,
                                    related_name='requisitions_approve',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-RequisitionApprove({self.id})"

    class Meta:
        db_table = 'requisitions_approve'


class RequisitionApproveItem(AuditClass):
    quantity = models.IntegerField()
    remarks = models.CharField(max_length=255, blank=True, null=True)

    requisition_approve = models.ForeignKey(
                            RequisitionApprove,
                            related_name='requisitions_approve_items',
                            on_delete=models.PROTECT)

    requisition_product = models.ForeignKey(
                                RequisitionProduct,
                                related_name='requisitions_approve_items',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition_approve.requisition.requisition_no})"\
               f"-RequisitionApprove({self.requisition_approve.id})"\
               f"-RequisitionApproveItem({self.id})"

    class Meta:
        db_table = 'requisitions_approve_items'


class RequisitionReject(AuditClass):
    remarks = models.CharField(max_length=255, blank=True, null=True)
    rejected_by = models.ForeignKey(User,
                                    related_name='requisitions_reject',
                                    on_delete=models.PROTECT)
    rejected_on = models.DateTimeField()

    requisition = models.ForeignKey(Requisition,
                                    related_name='requisitions_reject',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-RequisitionReject({self.id})"

    class Meta:
        db_table = 'requisitions_reject'


class RequisitionRejectItem(AuditClass):
    quantity = models.IntegerField()
    remarks = models.CharField(max_length=255, blank=True, null=True)

    requisition_reject = models.ForeignKey(
                            RequisitionReject,
                            related_name='requisitions_reject_items',
                            on_delete=models.PROTECT)

    requisition_product = models.ForeignKey(
                                RequisitionProduct,
                                related_name='requisitions_reject_items',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-RequisitionReject({self.requisition_reject.id})"\
               f"-RequisitionRejectItem({self.id})"

    class Meta:
        db_table = 'requisitions_reject_items'


class PurchaseRequest(AuditClass):
    status = models.CharField(
                        choices=[
                            (tag.value, tag.value) for tag in enums.RequisitionEnum],
                        default=enums.RequisitionEnum.PENDING.value,
                        max_length=50)
    pr_no = models.CharField(unique=True, max_length=255)
    po_no = models.CharField(max_length=255, blank=True, null=True)
    inv_no = models.CharField(max_length=255, blank=True, null=True)
    remarks = models.CharField(max_length=255, blank=True, null=True)

    requestor = models.ForeignKey(User,
                                  related_name='purchase_requests',
                                  on_delete=models.PROTECT)
    requested_date = models.DateTimeField()
    required_date = models.DateTimeField()

    requisition = models.ForeignKey(Requisition,
                                    related_name='purchase_requests',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-PurchaseRequest({self.pr_no})"

    class Meta:
        db_table = 'purchase_requests'


class PurchaseRequestItem(AuditClass):
    status = models.CharField(
                        choices=[
                            (tag.value, tag.value) for tag in enums.RequisitionEnum],
                        default=enums.RequisitionEnum.PENDING.value,
                        max_length=50)
    quantity = models.IntegerField(blank=True, null=True)
    remarks = models.CharField(max_length=255, blank=True, null=True)

    product = models.ForeignKey(Product,
                                related_name='purchase_request_items',
                                on_delete=models.PROTECT)
    purchase_request = models.ForeignKey(
                            PurchaseRequest,
                            related_name='purchase_request_items',
                            on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.purchase_request.requisition.requisition_no})"\
               f"-PurchaseRequest({self.purchase_request.pr_no})"\
               f"-PurchaseRequestItem({self.id})"

    class Meta:
        db_table = 'purchase_request_items'


class PurchaseRequestApprove(AuditClass):
    remarks = models.CharField(max_length=255, blank=True, null=True)
    approved_by = models.ForeignKey(User,
                                    related_name='purchase_requests_approve',
                                    on_delete=models.PROTECT)
    approved_on = models.DateTimeField()

    purchase_request = models.ForeignKey(
                                PurchaseRequest,
                                related_name='purchase_requests_approve',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.purchase_request.requisition.requisition_no})"\
               f"-PurchaseRequest({self.purchase_request.pr_no})"\
               f"-PurchaseRequestApprove({self.id})"

    class Meta:
        db_table = 'purchase_requests_approve'


class PurchaseRequestApproveItem(AuditClass):
    quantity = models.IntegerField()
    remarks = models.CharField(max_length=255, blank=True, null=True)

    purchase_request_approve = models.ForeignKey(
                                PurchaseRequestApprove,
                                related_name='purchase_requests_approve_items',
                                on_delete=models.PROTECT)

    purchase_request_item = models.ForeignKey(
                                PurchaseRequestItem,
                                related_name='purchase_requests_approve_items',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.purchase_request_approve.purchase_request.requisition.requisition_no})"\
               f"-PurchaseRequest({self.purchase_request_approve.purchase_request.pr_no})"\
               f"-PurchaseRequestApprove({self.purchase_request_approve.id})"\
               f"-PurchaseRequestApproveItem({self.id})"

    class Meta:
        db_table = 'purchase_requests_approve_items'


class PurchaseRequestReject(AuditClass):
    remarks = models.CharField(max_length=255, blank=True, null=True)
    rejected_by = models.ForeignKey(User,
                                    related_name='purchase_requests_rejects',
                                    on_delete=models.PROTECT)
    rejected_on = models.DateTimeField()

    purchase_request = models.ForeignKey(
                                PurchaseRequest,
                                related_name='purchase_requests_rejects',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.purchase_request.requisition.requisition_no})"\
               f"-PurchaseRequest({self.purchase_request.pr_no})"\
               f"-PurchaseRequestItem({self.id})"\
               f"-PurchaseRequestReject({self.id})"

    class Meta:
        db_table = 'purchase_requests_rejects'


class PurchaseRequestRejectItem(AuditClass):
    quantity = models.IntegerField()
    remarks = models.CharField(max_length=255, blank=True, null=True)

    purchase_request_reject = models.ForeignKey(
                                PurchaseRequestReject,
                                related_name='purchase_requests_reject_items',
                                on_delete=models.PROTECT)

    purchase_request_item = models.ForeignKey(
                                PurchaseRequestItem,
                                related_name='purchase_requests_reject_items',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.purchase_request_reject.purchase_request.requisition.requisition_no})"\
               f"-PurchaseRequest({self.purchase_request_reject.purchase_request.pr_no})"\
               f"-PurchaseRequestReject({self.purchase_request_reject.id})"\
               f"-PurchaseRequestRejectItem({self.id})"

    class Meta:
        db_table = 'purchase_requests_reject_items'


class Stock(AuditClass):
    stock_no = models.CharField(unique=True, max_length=255)
    serial_number = models.CharField(unique=True, max_length=255, null=True)

    product = models.ForeignKey(Product,
                                related_name='stocks',
                                on_delete=models.PROTECT)

    def __str__(self):
        return self.stock_no

    class Meta:
        db_table = 'stocks'


class Acquisition(AuditClass):
    acquisition_no = models.CharField(unique=True, max_length=255)
    acquired_date = models.DateTimeField()
    batch_no = models.IntegerField()
    po_no = models.CharField(max_length=255, blank=True, null=True)
    inv_no = models.CharField(max_length=255, blank=True, null=True)
    batch_price = models.DecimalField(max_digits=15, decimal_places=2)

    warehouse = models.ForeignKey(Warehouse,
                                  related_name='acquisitions',
                                  on_delete=models.PROTECT)
    supplier = models.ForeignKey(Supplier,
                                 related_name='acquisitions',
                                 on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.acquisition_no

    class Meta:
        db_table = 'acquisitions'


class AcquisitionItem(AuditClass):
    meta = models.TextField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2)
    currency = models.ForeignKey(Currency,
                                 related_name='acquisition_items',
                                 on_delete=models.PROTECT)
    item_condition = models.ForeignKey(ItemCondition,
                                       related_name='acquisition_items',
                                       on_delete=models.PROTECT)
    stock = models.ForeignKey(Stock,
                              related_name='acquisition_items',
                              on_delete=models.PROTECT)               
    acquisition = models.ForeignKey(Acquisition,
                                    related_name='acquisition_items',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Acquisition({self.acquisition.acquisition_no })"\
               f"-AcquisitionItem({self.id})"

    class Meta:
        db_table = 'acquisition_items'


class StockItem(AuditClass):
    status = models.CharField(
                        choices=[
                            (tag.value, tag.value) for tag in enums.StockEnum],
                        default=enums.StockEnum.IN.value,
                        max_length=50)

    price = models.DecimalField(max_digits=15, decimal_places=2)
    currency = models.ForeignKey(Currency,
                                 related_name='stock_items',
                                 on_delete=models.PROTECT)

    item_condition = models.ForeignKey(ItemCondition,
                                       related_name='stock_items',
                                       on_delete=models.PROTECT)
    warehouse = models.ForeignKey(Warehouse,
                                  related_name='stock_items_stock',
                                  on_delete=models.PROTECT)

    stock = models.ForeignKey(Stock,
                              related_name='stock_items_stock',
                              on_delete=models.PROTECT)
    acquisition_item = \
        models.ForeignKey(AcquisitionItem,
                          related_name='stock_items',
                          on_delete=models.PROTECT)

    def __str__(self):
        return f"Stock({self.stock.stock_no})"\
               f"-StockItem({self.id})"

    class Meta:
        db_table = 'stock_items'


class StockTake(AuditClass):
    status = models.CharField(
                        choices=[
                            (tag.value, tag.value) for tag in enums.StockTakeEnum],
                        default=enums.StockTakeEnum.OPEN.value,
                        max_length=50)
    stock_take_no = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    closed_on = models.DateTimeField(blank=True, null=True)
    closed_by = models.CharField(max_length=255, blank=True, null=True)
    close_remarks = models.CharField(max_length=255, blank=True, null=True)

    warehouse = models.ForeignKey(Warehouse,
                                  related_name='stock_takes',
                                  on_delete=models.PROTECT)

    def __str__(self):
        return self.stock_take_no

    class Meta:
        db_table = 'stock_takes'


class StockTakeItem(AuditClass):
    quantity = models.IntegerField()

    product = models.ForeignKey(Product,
                                related_name='stock_take_items',
                                on_delete=models.PROTECT)
    stock_take = models.ForeignKey(StockTake,
                                   related_name='stock_take_items',
                                   on_delete=models.PROTECT)

    def __str__(self):
        return f"StockTake({self.stock_take.stock_take_no})\
                -StockTakeItem({self.id})"

    class Meta:
        db_table = 'stock_take_items'


class StockTakeItemCount(AuditClass):
    quantity = models.IntegerField()
    count_on = models.DateTimeField(blank=False, null=False)

    count_by = models.ForeignKey(User,
                                 related_name='stock_take_item_counts',
                                 on_delete=models.PROTECT)
    stock_take_item = models.ForeignKey(StockTakeItem,
                                        related_name='stock_take_item_counts',
                                        on_delete=models.PROTECT)
    acquisition_item = models.ForeignKey(AcquisitionItem,
                                         related_name='stock_take_item_counts',
                                         on_delete=models.PROTECT)

    def __str__(self):
        return f"StockTake({self.stock_take_item.stock_take.stock_take_no})"\
               f"-StockTakeItem({self.stock_take_item.id})"\
               f"-StockTakeItemCount({self.id})"

    class Meta:
        db_table = 'stock_take_item_counts'


class Checkout(AuditClass):
    checkout_no = models.CharField(unique=True, max_length=255)
    remarks = models.CharField(max_length=255, blank=True, null=True)

    location = models.ForeignKey(ProjectLocation,
                                 related_name='checkouts_location',
                                 on_delete=models.PROTECT)
    location_from = models.ForeignKey(Warehouse,
                                      related_name='checkouts_location_from',
                                      on_delete=models.PROTECT)

    issued_by = models.ForeignKey(User,
                                  related_name='checkouts_issued_by',
                                  on_delete=models.PROTECT)

    checkeout_on = models.DateTimeField(blank=True, null=True)
    checkeout_by = models.ForeignKey(User,
                                     related_name='checkouts_checkout_by',
                                     on_delete=models.PROTECT)

    requisition = models.ForeignKey(Requisition,
                                    related_name='checkouts',
                                    on_delete=models.PROTECT)

    def __str__(self):
        return f"Requisition({self.requisition.requisition_no})"\
               f"-Checkout({self.checkout_no})"

    class Meta:
        db_table = 'checkouts'


class CheckoutItem(AuditClass):
    stock_item = models.ForeignKey(StockItem,
                                   related_name='checkout_items',
                                   on_delete=models.PROTECT)
    requisition_product = models.ForeignKey(RequisitionProduct,
                                            related_name='checkout_items',
                                            on_delete=models.PROTECT)
    checkout = models.ForeignKey(Checkout,
                                 related_name='checkout_items',
                                 on_delete=models.PROTECT)

    def __str__(self):
        return f"Checkout({self.checkout.checkout_no})"\
               f"-RequisitionProduct({self.requisition_product.product.product_no})"\
               f"-CheckoutItem({self.id})"

    class Meta:
        db_table = 'checkout_items'


class CheckoutErrorReturn(AuditClass):
    remarks = models.CharField(max_length=255, blank=True, null=True)

    return_by = models.ForeignKey(User,
                                  related_name='checkout_error_returns',
                                  on_delete=models.PROTECT)
    return_on = models.DateTimeField(blank=True, null=True)

    checkout = models.ForeignKey(Checkout,
                                 related_name='checkout_error_returns',
                                 on_delete=models.PROTECT)

    def __str__(self):
        return f"Checkout({self.checkout.checkout_no})"\
               f"-CheckoutErrorReturn({self.id})"

    class Meta:
        db_table = 'checkout_error_returns'


class CheckoutErrorReturnItem(AuditClass):
    checkout_item = models.ForeignKey(CheckoutItem,
                                      related_name='checkout_error_returns',
                                      on_delete=models.PROTECT)
    checkout_error_return = models.ForeignKey(
                                CheckoutErrorReturn,
                                related_name='checkout_error_returns',
                                on_delete=models.PROTECT)

    def __str__(self):
        return f"Checkout({self.checkout_error_return.checkout.checkout_no})"\
               f"-CheckoutErrorReturn({self.checkout_error_return.id})"\
               f"-CheckoutErrorReturnItem({self.id})"

    class Meta:
        db_table = 'checkout_error_return_items'


class CheckoutReturn(AuditClass):
    remarks = models.CharField(max_length=255, blank=True, null=True)

    return_by = models.ForeignKey(User,
                                  related_name='checkout_returns',
                                  on_delete=models.PROTECT)
    return_on = models.DateTimeField(blank=True, null=True)

    warehouse = models.ForeignKey(Warehouse,
                                  related_name='checkout_returns',
                                  on_delete=models.PROTECT)
    project_location = models.ForeignKey(ProjectLocation,
                                         related_name='checkout_returns',
                                         on_delete=models.PROTECT,
                                         blank=True, null=True)

    # checkout = models.ForeignKey(Checkout,
    #                              related_name='checkout_returns',
    #                              on_delete=models.PROTECT)

    def __str__(self):
        return f"Checkout({self.checkout.checkout_no})"\
               f"-CheckoutReturn({self.id})"

    class Meta:
        db_table = 'checkout_returns'


class CheckoutReturnItem(AuditClass):
    checkout_item = models.ForeignKey(CheckoutItem,
                                      related_name='checkout_return_items',
                                      on_delete=models.PROTECT)
    checkout_return = models.ForeignKey(CheckoutReturn,
                                        related_name='checkout_return_items',
                                        on_delete=models.PROTECT)

    def __str__(self):
        return f"Checkout({self.checkout_return.checkout.checkout_no})"\
               f"-CheckoutItem({self.checkout_item})"\
               f"-CheckoutReturnItem({self.id})"

    class Meta:
        db_table = 'checkout_return_items'


class AssetGroup(AuditClass):
    parent = models.ForeignKey('AssetGroup',
                               related_name='asset_group_subs',
                               on_delete=models.PROTECT,
                               blank=True, null=True)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'asset_groups'


class AssetType(AuditClass):
    parent = models.ForeignKey('AssetType',
                               related_name='asset_type_subs',
                               on_delete=models.PROTECT,
                               blank=True, null=True)
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)
    form_details = models.TextField(blank=True, null=True)

    asset_group = models.ForeignKey(Company,
                                    related_name='asset_types',
                                    on_delete=models.PROTECT)

    class Meta:
        db_table = 'asset_types'


class Asset(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)

    # AMS Fields
    primary_id = models.CharField(unique=True, max_length=255)
    secondary_id = models.CharField(unique=True, max_length=255)
    total_price = models.DecimalField(max_digits=15, decimal_places=2)
    date_of_purchase = models.DateTimeField()
    cr_reference_number = models.CharField(max_length=255)
    total_quantity_in_stock = models.IntegerField()

    description = models.CharField(max_length=255, blank=True, null=True)

    unitmaterial = models.ForeignKey(UnitMaterial,
                                     related_name='assets',
                                     on_delete=models.PROTECT)

    asset_type = models.ForeignKey(AssetType,
                                   related_name='assets',
                                   on_delete=models.PROTECT)

    class Meta:
        db_table = 'assets'


class AssetStocks(AuditClass):
    code = models.CharField(unique=True, max_length=255)
    name = models.CharField(unique=True, max_length=255)
    description = models.CharField(max_length=255, blank=True, null=True)

    asset = models.ForeignKey(Company,
                              related_name='asset_stocks',
                              on_delete=models.PROTECT)

    acquisition_item = models.ForeignKey(AcquisitionItem,
                                         related_name='asset_stocks',
                                         on_delete=models.PROTECT,
                                         blank=True, null=True)

    class Meta:
        db_table = 'asset_stocks'


class AuthToken(models.Model):
    ref = models.CharField(max_length=255)
    token = models.TextField()
    passcode = models.IntegerField(blank=False, null=True)
    timeout = models.IntegerField()
    is_active = models.BooleanField(default=False)
    user = models.ForeignKey(User,
                             related_name='auth_access_token',
                             on_delete=models.PROTECT)

    class Meta:
        db_table = 'auth_access_token'
