from src.helper import decorators
from rest_framework import status
from rest_framework.views import APIView
from src.entities.models import User
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from datetime import datetime
from src.applicationlayer.serializers import UserSerializer


class Login(ObtainAuthToken):
    @decorators.error_safe
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        if not created:
            token.created = datetime.now()
            token.save()

        return Response({
            'token': token.key,
            # 'user_id': user.pk,
            # 'email': user.email
        })


class Logout(APIView):
    @decorators.error_safe
    def post(self, request, username=None, *args, **kwargs):
        existingUser = User.objects.filter(username=username).first()
        if existingUser:
            existingToken = Token.objects.filter(user=existingUser).first()
            if existingToken:
                existingToken.delete()
                return Response(data={"detail": "User was logged out"},
                                status=status.HTTP_200_OK)
            else:
                return Response(data={"detail": "User session not found"},
                                status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(data={"detail": "User not found"},
                            status=status.HTTP_404_NOT_FOUND)


class RefreshToken(APIView):
    @decorators.error_safe
    def post(self, request, token=None, *args, **kwargs):
        existingToken = Token.objects.filter(key=token).first()
        if existingToken:
            if existingToken.user == request.user:
                existingToken.created = datetime.now()
                existingToken.save()
                return Response(data={"detail": "Token refreshed"},
                                status=status.HTTP_200_OK)
            else:
                return Response(data={"detail": "Token user not match"},
                                status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response(data={"detail": "Token not found"},
                            status=status.HTTP_404_NOT_FOUND)


class CurrentUser(APIView):
    @decorators.error_safe
    def get(self, request, token=None, *args, **kwargs):
        serializer = UserSerializer

        serializer.Meta.fields = ('id',
                                  'username',
                                  'first_name',
                                  'last_name',
                                  'email',
                                  'nric',
                                  'is_superuser')

        serializer = serializer(request.user)

        data = serializer.data

        return Response(data=serializer.data,
                        status=status.HTTP_200_OK)
