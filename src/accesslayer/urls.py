from django.urls import path, re_path

from src.accesslayer import views

urlpatterns = (
    path(r'login/', views.Login.as_view(), name="API Login"),
    re_path(r'^logout/(?P<username>\w+)/$', views.Logout.as_view(), name="API Logout"),
    re_path(r'^refresh-token/(?P<token>\w+)/$', views.RefreshToken.as_view(), name="Refresh Token"),
    path(r'current-user/', views.CurrentUser.as_view(), name="Current User"),
)
