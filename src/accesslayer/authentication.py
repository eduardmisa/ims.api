from src.middleware.current_user.data import set_current_user
from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions
from datetime import datetime, timedelta


class AppTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'

    def authenticate_credentials(self, key):

        user, token = super(AppTokenAuthentication,
                            self).authenticate_credentials(key)

        if token.created < datetime.now() - timedelta(minutes=30):
            token.delete()
            raise exceptions.AuthenticationFailed('Token has expired')

        set_current_user(user)

        return (user, token)
